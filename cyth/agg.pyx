# distutils: language=c++
# cython: language_level=3
# cython: embedsignature = True

cimport cython

import numpy as np
cimport numpy as np

from libcpp.vector cimport vector
from agg cimport CTarget, CConstraint, CAggregation, CAggregator, NumpyMatrix, FT, tREAL, tINT, tUINT, AggMode, ConstraintType, ConstraintLogic, extractFromSequence

NEG_INF = - 1e9
POS_INF =   1e9
NEG_EPS = - 1e-10
POS_EPS =   1e-10


cdef NumpyMatrix[FT] from_NumpyMatrix(np.ndarray[FT,ndim=2] arr):
    cdef long* p_sha = <long*>&arr.shape[0]
    cdef long* p_str = <long*>&arr.strides[0]
    cdef bint isC = arr.flags['C_CONTIGUOUS']
    return NumpyMatrix[FT](<FT*>arr.data, p_sha, p_str, isC)


cdef class Target:
    cdef CTarget* cobj;

    def __cinit__(self, int ay, AggMode af, int al, int au):
        self.cobj = new CTarget(ay, af, al, au)

    @property
    def ay(self):
        return self.cobj.ay
    @ay.setter
    def ay(self, ay):
        self.cobj.ay = ay

    @property
    def af(self):
        return self.cobj.af
    @af.setter
    def af(self, af):
        self.cobj.af = af

    @property
    def al(self):
        return self.cobj.al
    @al.setter
    def al(self, al):
        self.cobj.al = al

    @property
    def au(self):
        return self.cobj.au
    @au.setter
    def au(self, au):
        self.cobj.au = au

    def __str__(self):
        return self.cobj.toString().decode('utf-8') #'("Target ay = %d af = %d al = %d au = %d" % (self.ay, self.af, self.al, self.au))


cdef class Constraint:
    cdef CConstraint* cobj;

    def __cinit__(self, ConstraintType ctype, int aci = -1, tREAL acl = 0.0, tREAL acu = 0.0, vector[tREAL] vset = [], bint neg = False):
        if ctype == ConstraintType.VALUE_SET:
            self.cobj = new CConstraint(<ConstraintType>ctype, aci, vset, <int>neg)
        else:
            self.cobj = new CConstraint(<ConstraintType>ctype, aci, acl, acu, <int>neg)

    @property
    def aci(self):
        return self.cobj.aci
    @aci.setter
    def aci(self, aci):
        self.cobj.aci = aci

    @property
    def acl(self):
        return self.cobj.acl
    @acl.setter
    def acl(self, acl):
        self.cobj.acl = acl

    @property
    def acu(self):
        return self.cobj.acu
    @acu.setter
    def acu(self, acu):
        self.cobj.acu = acu

    @property
    def neg(self):
        return self.cobj.neg
    @neg.setter
    def neg(self, neg):
        self.cobj.neg = neg

    def __str__(self):
        return self.cobj.toString().decode('utf-8')


cdef class Aggregation:
    cdef CAggregation* cobj;

    def __cinit__(self, Target target, list constraints, ConstraintLogic logic = ConstraintLogic.AND):
        cdef CConstraint _co
        cdef vector[CConstraint] _consts
        for obj in constraints:
            _co = cython.operator.dereference((<Constraint>obj).cobj)
            _consts.push_back(_co)
        self.cobj = new CAggregation(cython.operator.dereference(target.cobj), _consts, logic)

    @property
    def target(self):
        cdef Target tar = Target(self.cobj.target.ay, self.cobj.target.af, self.cobj.target.al, self.cobj.target.au)
        return tar

    @property
    def constraints(self):
        cdef list pyconsts = []
        cdef CConstraint conny
        cdef Constraint pyconny
        for i in range(self.cobj.constraints.size()):
            conny = self.cobj.constraints[i]
            pyconny = Constraint(conny.ctype, conny.aci, conny.acl, conny.acu)
            pyconsts.append(pyconny)
        return pyconsts

    def __str__(self):
        return self.cobj.toString().decode('utf-8')


cdef class Aggregator:
    cdef CAggregator* cobj
    cdef int F

    def __cinit__(self, np.ndarray[tREAL,ndim=2] arr, long groupColIndex):
        self.F = arr.shape[1]
        self.cobj = new CAggregator(from_NumpyMatrix[tREAL](arr), groupColIndex)

    def extract(self, list aggregations, tREAL default_value = np.nan, bint fCuda = False, bint fParallel = False, bint fFreeDevice = True, bint fVerbose = False):
        checkAggregations(aggregations, self.F)

        cdef np.ndarray[tREAL, ndim=2] Z = np.zeros((self.getNumItems(), len(aggregations)), dtype=np.float32)
        cdef vector[CAggregation*] _aggs
        # cdef vector[CAggregation] _aggs
        cdef CAggregation* _ag
        for pyagg in aggregations:
            # _ag = cython.operator.dereference((<Aggregation>pyagg).cobj)
            _ag = (<Aggregation>pyagg).cobj
            _aggs.push_back(_ag)
        cdef NumpyMatrix[tREAL] _Z = from_NumpyMatrix[tREAL](Z)
        self.cobj.extract(_aggs, _Z, default_value, fCuda, fParallel, fFreeDevice, fVerbose)
        return Z

    def getGroup(self, tREAL group):
        return self.cobj.getGroup(group)

    def getPredecessor(self, tUINT index):
        return self.cobj.getPredecessor(index)

    def getSuccessor(self, tUINT index):
        return self.cobj.getSuccessor(index)

    def getPredecessors(self, tUINT index):
        return self.cobj.getPredecessorsOf(index)

    def getSuccessors(self, tUINT index):
        return self.cobj.getSuccessorsOf(index)

    def getNumGroups(self):
        return self.cobj.getNumGroups()

    def getNumItems(self):
        return self.cobj.getNumItems()

    def getGroupColIndex(self):
        return self.cobj.getGroupColIndex()


def checkAggregations(list aggregations, int F):
    for ai, a in enumerate(aggregations):
        ay = a.target.ay
        if (ay < 0) or (F <= ay):
            raise IndexError("Aggregation %d :: Target :: Column index out of bounds (%d not in [%d, %d])" % (ai, ay, 0, F-1))
        for ci, c in enumerate(a.constraints):
            aci = c.aci
            if (aci < 0) or (F <= aci):
                raise IndexError("Aggregation %d :: Constraint %d :: Column index out of bounds (%d not in [%d, %d])" % (ai, ci, aci, 0, F-1))


def extract(np.ndarray[tREAL, ndim=2] X, list aggregations, tREAL default_value = np.nan, bint fCuda = False, bint fParallel = False, bint fVerbose = False):
    checkAggregations(aggregations, X.shape[1])

    cdef np.ndarray[tREAL, ndim=2] Z = np.zeros((X.shape[0], len(aggregations)), dtype=np.float32)
    cdef vector[CAggregation*] _aggs
    # cdef vector[CAggregation] _aggs
    cdef CAggregation* _ag
    for pyagg in aggregations:
        # _ag = cython.operator.dereference((<Aggregation>pyagg).cobj)
        _ag = (<Aggregation>pyagg).cobj
        _aggs.push_back(_ag)
    cdef NumpyMatrix[tREAL] _X = from_NumpyMatrix[tREAL](X)
    cdef NumpyMatrix[tREAL] _Z = from_NumpyMatrix[tREAL](Z)
    extractFromSequence(_X, _aggs, _Z, default_value, fCuda, fParallel, fVerbose)
    return Z






from lark import Transformer
from lark import Lark


#"IN" range
# BNF grammar for aggregation specification language
parser = Lark(r"""
    agg    : target ["WHERE" LOGIC cons]
    target : AGGOP COLIDX "FROM" idx "TO" idx
    AGGOP  : "COUNT"
           | "SUM"  
           | "MEAN"
           | "MIN" 
           | "MAX"
           | "SPAN"
           | "MEAN_SPAN"
    COLIDX : "*" | UINT
    LOGIC  : "ANY" | "ALL"
    cons   : (cdelta | cvalue | cset)*
    cdelta : [NEG] "DELTA" "(" UINT ")" cmp
    cvalue : [NEG] "VALUE" "(" UINT ")" cmp
    cset   : [NEG] "VALUE" "(" UINT ")" "IN" set
    cmp    : CMPOP REAL | IN range
    CMPOP  : "<=" | ">=" | "<" | ">" | "==" | "!="
    range  : "[" REAL "," REAL "]"
    set    : "{" [REAL ("," REAL)*] "}"
    NEG    : "NOT"
    IN     : "IN"
    idx    : INF | INT
    INF    : ["-" | "+"] "INF"

    %import common.SIGNED_NUMBER -> REAL
    %import common.SIGNED_INT -> INT
    %import common.INT -> UINT
    %import common.WS
    %ignore WS

    """, start='agg')

class MyTransformer(Transformer):

    def agg(self, items):

        [af, ay, al, au] = list(items[0])
        #print(af, ay, al, au)

        _ay = 0
        if(ay.value != '*'):
            _ay = int(ay.value)

        _af = AggMode.COUNT
        if(af.value == 'COUNT'):
            _af = AggMode.COUNT
        elif(af.value == 'SUM'):
            _af = AggMode.SUM
        elif(af.value == 'MEAN'):
            _af = AggMode.MEAN
        elif(af.value == 'MIN'):
            _af = AggMode.MIN
        elif(af.value == 'MAX'):
            _af = AggMode.MAX
        elif(af.value == 'SPAN'):
            _af = AggMode.SPAN
        elif(af.value == 'MEAN_SPAN'):
            _af = AggMode.MEAN_SPAN

        tar = Target(_ay, _af, al, au) # add logic

        logic = ConstraintLogic.AND
        constraints = []
        if len(items) == 3:
            if(items[1].value == 'ANY'):
                logic = ConstraintLogic.OR
            else:
                logic = ConstraintLogic.AND

            constraints = items[2]

        return Aggregation(tar, constraints, logic = logic)

    def target(self, items):
        return list(items)

    def cons(self, items):
        return list(items)

    def cdelta(self, items):
        #print(items)
        ritems = items[-2:]
        neg = len(items) > len(ritems)
        CMP = ritems[-1]
        co = Constraint(ConstraintType.DELTA_INTERVAL, int(ritems[0].value), CMP['low'], CMP['high'], neg= neg ^ CMP['neg'])
        return co

    def cvalue(self, items):
        ritems = items[-2:]
        neg = len(items) > len(ritems)
        CMP = ritems[-1]
        co = Constraint(ConstraintType.VALUE_INTERVAL, int(ritems[0].value), CMP['low'], CMP['high'], neg= neg ^ CMP['neg'])
        return co

    def cset(self, items):
        ritems = items[-2:]
        neg = len(items) > len(ritems)
        co = Constraint(ConstraintType.VALUE_SET, int(ritems[0].value), vset=ritems[1], neg=neg)
        return co

    def cmp(self, items):
        op = items[0].value
        if(op == "IN"):
            return {'neg': False, 'low': items[1][0], 'high': items[1][1]}

        v = float(items[1].value)
        if(op == "<="):
            return {'neg': False, 'low': NEG_INF, 'high': v}
        elif(op == "<"):
            return {'neg': True,  'low': v, 'high': POS_INF}
        elif(op == ">="):
            return {'neg': False, 'low': v, 'high': POS_INF}
        elif(op == ">"):
            return {'neg': True,  'low': NEG_INF, 'high': v}
        elif(op == "=="):
            return {'neg': False, 'low': v, 'high': v}
        elif(op == "!="):
            return {'neg': True,  'low': v, 'high': v}
        else:
            return []

    def idx(self, items):
        return int(items[0])

    def INF(self, items):
        if items == "-INF":
            return NEG_INF
        if items in ["INF", "+INF"]:
            return POS_INF

    def INT(self, items):
        return int(items)

    def range(self, items):
        return [float(items[0].value), float(items[1].value)]

    def set(self, items):
        return [float(tok.value) for tok in items]


def fromQuery(queryStr):
    agg0 = MyTransformer().transform(parser.parse(queryStr))
    return agg0


# # BNF grammar for aggregation specification language
# parser = Lark(r"""
#     agg    : target ["WHERE" LOGIC cons]
#     target : AGGOP COLIDX "FROM" INT "TO" INT
#     AGGOP  : "COUNT"
#            | "SUM"
#            | "MEAN"
#            | "MIN"
#            | "MAX"
#            | "SPAN"
#            | "MEAN_SPAN"
#     COLIDX : "*" | UINT
#     LOGIC  : "ANY" | "ALL"
#     cons   : (cdelta | cvalue | cset)*
#     cdelta : [NEG] "DELTA" "(" UINT ")" cmp
#     cvalue : [NEG] "VALUE" "(" UINT ")" cmp
#     cset   : [NEG] "VALUE" "(" UINT ")" "IN" set
#     cmp    : CMPOP REAL | IN range
#     CMPOP  : "<=" | ">=" | "<" | ">" | "==" | "!="
#     range  : "[" REAL "," REAL "]"
#     set    : "{" [REAL ("," REAL)*] "}"
#     NEG    : "NOT"
#     IN     : "IN"
#
#     %import common.SIGNED_NUMBER -> REAL
#     %import common.SIGNED_INT -> INT
#     %import common.INT -> UINT
#     %import common.WS
#     %ignore WS
#
#     """, start='agg')
#
# class MyTransformer(Transformer):
#
#     def agg(self, items):
#
#         [af, ay, al, au] = list(items[0])
#
#         _ay = 0
#         if(ay.value != '*'):
#             _ay = int(ay.value)
#
#         _af = AggMode.COUNT
#         if(af.value == 'COUNT'):
#             _af = AggMode.COUNT
#         elif(af.value == 'SUM'):
#             _af = AggMode.SUM
#         elif(af.value == 'MEAN'):
#             _af = AggMode.MEAN
#         elif(af.value == 'MIN'):
#             _af = AggMode.MIN
#         elif(af.value == 'MAX'):
#             _af = AggMode.MAX
#         elif(af.value == 'SPAN'):
#             _af = AggMode.SPAN
#         elif(af.value == 'MEAN_SPAN'):
#             _af = AggMode.MEAN_SPAN
#
#         tar = Target(_ay, _af, int(al.value), int(au.value)) # add logic
#
#         logic = ConstraintLogic.AND
#         constraints = []
#         if len(items) == 3:
#             if(items[1].value == 'ANY'):
#                 logic = ConstraintLogic.OR
#             else:
#                 logic = ConstraintLogic.AND
#
#             constraints = items[2]
#
#         return Aggregation(tar, constraints, logic = logic)
#
#     def target(self, items):
#         return list(items)
#
#     def cons(self, items):
#         return list(items)
#
#     def cdelta(self, items):
#         ritems = items[-2:]
#         neg = len(items) > len(ritems)
#         CMP = ritems[-1]
#         co = Constraint(ConstraintType.DELTA_INTERVAL, int(ritems[0].value), CMP['low'], CMP['high'], neg= neg ^ CMP['neg'])
#         return co
#
#     def cvalue(self, items):
#         ritems = items[-2:]
#         neg = len(items) > len(ritems)
#         CMP = ritems[-1]
#         co = Constraint(ConstraintType.VALUE_INTERVAL, int(ritems[0].value), CMP['low'], CMP['high'], neg= neg ^ CMP['neg'])
#         return co
#
#     def cset(self, items):
#         ritems = items[-2:]
#         neg = len(items) > len(ritems)
#         co = Constraint(ConstraintType.VALUE_SET, int(ritems[0].value), vset=ritems[1], neg=neg)
#         return co
#
#     def cmp(self, items):
#         op = items[0].value
#         if(op == "IN"):
#             return {'neg': False, 'low': items[1][0], 'high': items[1][1]}
#
#         v = float(items[1].value)
#         if(op == "<="):
#             return {'neg': False, 'low': NEG_INF, 'high': v}
#         elif(op == "<"):
#             return {'neg': True,  'low': v, 'high': POS_INF}
#         elif(op == ">="):
#             return {'neg': False, 'low': v, 'high': POS_INF}
#         elif(op == ">"):
#             return {'neg': True,  'low': NEG_INF, 'high': v}
#         elif(op == "=="):
#             return {'neg': False, 'low': v, 'high': v}
#         elif(op == "!="):
#             return {'neg': True,  'low': v, 'high': v}
#         else:
#             return []
#
#     def range(self, items):
#         return [float(items[0].value), float(items[1].value)]
#
#     def set(self, items):
#         return [float(tok.value) for tok in items]

