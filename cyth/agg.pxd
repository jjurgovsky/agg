# distutils: language=c++
# cython: language_level=3


cimport numpy as np
from libcpp.string cimport string
from libcpp.vector cimport vector




cdef extern from "../src/types.h":
    ctypedef float tREAL
    ctypedef long tINT
    ctypedef unsigned long tUINT


ctypedef fused FT:
    tREAL
    tINT
    tUINT
    np.int32_t
    np.int64_t
    np.float32_t
    np.float64_t




cdef extern from "../src/NumpyMatrix.h" namespace "ccfd::numpy":
    cdef cppclass NumpyMatrix[T]:
        NumpyMatrix() except +
        NumpyMatrix(T*, long*, long*, bint c_contiguous) except +


cdef extern from "../src/agg.h" namespace "ccfd::cuda":
    cpdef enum AggMode:
        SUM,
        MEAN,
        COUNT,
        MIN,
        MAX,
        SPAN,
        MEAN_SPAN


cdef extern from "../src/agg.h" namespace "ccfd::cuda":
    cpdef enum ConstraintType:
        DELTA_INTERVAL,
        VALUE_INTERVAL,
        VALUE_SET

cdef extern from "../src/agg.h" namespace "ccfd::cuda":
    cpdef enum ConstraintLogic:
        AND,
        OR


cdef extern from "../src/Aggregation.h" namespace "ccfd::agg":
    cdef cppclass CTarget:
        int ay
        AggMode af
        int al
        int au
        CTarget(int, AggMode, int, int) except +
        string toString()

    cdef cppclass CConstraint:
        ConstraintType ctype
        int aci
        tREAL acl
        tREAL acu
        int neg
        vector[tREAL] vset

        CConstraint() except +
        CConstraint(ConstraintType, int, tREAL, tREAL, int) except +
        CConstraint(ConstraintType, int, vector[tREAL], int) except +
        string toString()

    cdef cppclass CAggregation:
        CTarget target
        vector[CConstraint] constraints

        CAggregation() except +
        CAggregation(CTarget, vector[CConstraint], ConstraintLogic) except +
        string toString()

    cdef cppclass CAggregator:
        CAggregator(const NumpyMatrix[tREAL]&, long) except +
        void extract(vector[CAggregation*], NumpyMatrix[tREAL]&, tREAL, bint, bint, bint, bint)
        vector[tUINT] getGroup(tREAL group)
        long getPredecessor(tUINT index)
        long getSuccessor(tUINT index)
        vector[tUINT] getPredecessorsOf(tUINT index)
        vector[tUINT] getSuccessorsOf(tUINT index)
        tUINT getNumGroups()
        tUINT getNumItems()
        long getGroupColIndex()


    cdef void extractFromSequence(const NumpyMatrix[tREAL]&, vector[CAggregation*], NumpyMatrix[tREAL]&, tREAL, bint, bint, bint)

