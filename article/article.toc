\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {paragraph}{Assumption 1:}{2}{section*.4}
\contentsline {paragraph}{Assumption 2:}{2}{section*.5}
\contentsline {section}{\numberline {2}Aggregation}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Selecting Context Transactions}{3}{subsection.2.1}
\contentsline {paragraph}{Constraint Types}{4}{section*.7}
\contentsline {subsection}{\numberline {2.2}Aggregation}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Illustration}{6}{subsection.2.3}
\contentsline {section}{\numberline {3}Implementation}{7}{section.3}
\contentsline {section}{\numberline {4}Evaluation}{9}{section.4}
\contentsline {section}{\numberline {5}Conclusion}{10}{section.5}
\contentsline {paragraph}{Acknowledgement}{10}{section*.10}
\contentsline {section}{\numberline {6}Appendix}{10}{section.6}
\contentsline {section}{References}{10}{figure.2}
