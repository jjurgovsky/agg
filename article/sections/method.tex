\section{Aggregation} \label{sec:method}
\todo{check notation and align with rest}
The value of an aggregation feature is calculated for each transaction. For purposes of the following explanation we call this single transaction for which we calculate the aggregation value the \emph{pivot} transaction $\mathbf{x}_i \in \mathbb{R}^d$. An aggregation is composed of two processes: (i) Selecting \emph{context} transactions in the neighborhood around the pivot and (ii) aggregating some feature of the selected transactions using an operator, e.g. sum, min, max, etc. In order to refer to any arbitrary component in $\mathbf{x}$ for transactions  $i,j$, we use the following notation:
\begin{align*}
& x_i, x_j & \in \mathbb{R} & \qquad \textit{(value of a feature in transactions i, j)}\\
& d_{ji} = x_j - x_i & \in \mathbb{R} & \qquad \textit{(value difference of two transactions j, i)}\\
& z_i &\in \mathbb{R} & \qquad \textit{(value of aggregation in transaction i)}
\end{align*}

\subsection{Selecting Context Transactions}
We select transactions around the pivot by checking each transaction against a set of boolean constraints. Any transaction that complies with the constraints gets selected. Our framework features three kinds of constraints: Constraints that allow to select transactions based on by how much they differ from the pivot transaction (ii) Constraints that select transactions based on whether their values fall into some interval and (iii) constraints that select transactions based on whether their values are contained in some given set. Each constraint is defined via parameters: In case of interval constraints, we have to specify the lower and upper endpoints and in case of the value set constraint we have to specify the values in the set.

\begin{align}
& d_{ji} &\in [a_\delta, b_\delta]  		& \qquad \textit{(delta interval)}\\
& x_j &\in [a_v, b_v]				     	& \qquad \textit{(value interval)}\\
& x_j &\in \{a_0, a_1, a_2, \dots \}	& \qquad \textit{(value set)}
\end{align}

\noindent
We define the following primitive constraint functions. Each of them maps a pair of values to either $0$ (\emph{no match}) or $1$ (\emph{match}):

\begin{align}
\delta, \nu, \sigma \colon \mathbb{R} \times \mathbb{R} &\to \{0, 1 \}\\
\delta_\theta (x_j, x_i) &= \mathbbm{1}_{[a_\delta, b_\delta ]}(x_j - x_i),  \qquad \theta = \{a_\delta, b_\delta \} \\
\nu_\theta (x_j, x_i)      &= \mathbbm{1}_{[a_v, b_v]}(x_j), \qquad \qquad \theta = \{a_\nu, b_\nu \}\\
\sigma_\theta (x_j, x_i) &= \mathbbm{1}_{\{a_0, a_1, a_2, \dots \}}(x_j), \qquad \theta = \{a_0, a_1, a_2, \dots \}
\end{align}

\noindent
Notice that only the delta interval constraint function depends on some pivot $i$, whereas the value interval and set indicator functions only depend on $j$. Here, $i$ indexes the transaction for which we are calculating the aggregation and $j$ indexes any other transaction in its neighbourhood. In literature, aggregation features that use such delta constraints are called \emph{relative aggregations} because their values depend on some \emph{pivot} transaction $i$.

Finally, a constraint $c_{ \{ r, \gamma_{\theta} \} } \colon \mathbb{R}^d \times \mathbb{R}^d \to \{0, 1\}$ is parameterized by a feature index $r \in \{1, \dots, d \}$ and a constraint function $\gamma_\theta \in \{ \delta_\theta, \nu_\theta, \sigma_\theta \}$ and it indicates whether transaction $\mathbf{x}_j$ matches transaction $\mathbf{x}_i$ in feature $r$ w.r.t. constraint function $\gamma_\theta$:

\begin{align}
c_{ \{ r, \gamma_{\theta} \} }(\mathbf{x}_j, \mathbf{x}_i) &= 
\begin{cases}
 \delta_\theta ( \mathbf{x}_{jr}, \mathbf{x}_{ir}),  & \text{if}\ \gamma_\theta = \delta_\theta \\
 \nu_\theta ( \mathbf{x}_{jr}, \mathbf{x}_{ir}),  & \text{if}\ \gamma_\theta = \nu_\theta \\
 \sigma_\theta ( \mathbf{x}_{jr}, \mathbf{x}_{ir}),  & \text{if}\ \gamma_\theta = \sigma_\theta \\
\end{cases}
\end{align}
 
 
 
\paragraph{Constraint Types} The three constraint types $\delta_\theta , \nu_\theta, \sigma_\theta$ possess different semantics and thereby qualify for different purposes: 
\begin{itemize}
\item A delta interval constraint  $c_{ \{ r, \gamma_{\theta} \} }$ evaluates to $1$ if $\mathbf{x}_{jr}$ is somewhat close to the value of some pivot $\mathbf{x}_{ir}$ in the sense $\mathbf{x}_{jr} \in [\mathbf{x}_{ir} + a_\delta, \mathbf{x}_{ir} + b_\delta]$. Such constraints allow to search the neighborhood around the pivot for transactions with "similar" values. Examples include a selection of transactions from the recent past before the pivot or a selection of transactions with properties like the pivot. Depending on the feature the constraint is defined on and the interval endpoints, the semantics of delta interval constraints may vary.
\item A value interval constraint  $c_{ \{ r, \nu{\theta} \} }$ evaluates to $1$ if $\mathbf{x}_{jr}$ takes values between $a_\nu \leq \mathbf{x}_{jr} \leq b_\nu$. Such constraints select transactions in an absolute manner without considering any relation to a pivot. Therefore, these constraints are useful when we want to include transactions in the selection based solely on value they assume in some feature. Examples include the selection of transactions issued during lunchtime or the selection of transactions within some price range.
\item A value set constraint  $c_{ \{ r, \sigma{\theta} \} }$ evaluates to $1$ if $\mathbf{x}_{jr}$ is one of the values in $\{a_0, a_1, \dots \}$. Such constraints are very similar to the value interval constraints. The only difference is that here we look out for particular values in a finite set whereas the value interval constraint accepts anything within the interval endpoints. Such constraints are especially useful for selections on categorical features.
\end{itemize}
  

Now, we consider a concrete instantiation of several constraints. For notational convenience, we simply collapse each constraint's parametrization into the index $k$ and refer to the constraint as $c_k \in \mathcal{C} = \{c_0, c_1, \dots, c_K\}$.

When there is more than one constraint involved in the selection, we have to decide whether we want to combine them via logical conjunction or disjunction. Since we defined constraints as indicator functions, we formulate the logical combination as product over indicator functions to form a joint binary matching function $m_{ \{ \land, \mathcal{C} \} } (\mathbf{x}_j, \mathbf{x}_i ) \colon \mathbb{R}^d \times \mathbb{R}^d \to \{0, 1\}$  from multiple constraints. We start with the logical conjunction, that can be expressed as: 

\begin{align}
%m_{ \{ \land, \mathcal{C} \} } &\colon \mathbb{R}^d \times \mathbb{R}^d \to \{0, 1\}\\
m_{ \{ \land, \mathcal{C} \} } (\mathbf{x}_j, \mathbf{x}_i ) &= \prod_k  c_k(\mathbf{x}_j, \mathbf{x}_i)
\end{align}

\noindent
We might want to consider not only positive constraints $c_k(\mathbf{x}_j, \mathbf{x}_i)$ but also the negation of a constraint $(1- c_k(\mathbf{x}_j, \mathbf{x}_i) )$. Therefore, we equip each constraint with a binary indicator parameter $\beta_k \in \{0, 1\}$ to signal whether a constraint should be negated. Thus, the \emph{negation}-aware version $m_{ \{ \land, \mathcal{C}, \beta \} } \colon \mathbb{R}^d \times \mathbb{R}^d \to \{0, 1\}$ is:

\begin{align}
\label{eq:negconstraint}
%m_{\{\land, \mathcal{C}, \mathbf{\beta}\}} \colon \mathbb{R}^d \times \mathbb{R}^d &\to \{0, 1\}\\
m_{\{\land, \mathcal{C}, \mathbf{\beta}\}}(\mathbf{x}_j, \mathbf{x}_i) &= \prod_k  \underbrace{\beta_k (1 - c_k(\mathbf{x}_j, \mathbf{x}_i)) + (1 - \beta_k) c_k(\mathbf{x}_j, \mathbf{x}_i)}_{\hat{c}_k(\mathbf{x}_j, \mathbf{x}_i)}
\end{align}

\noindent
From boolean algebra we know that a boolean disjunction can be expressed in terms of the negation of a boolean conjunction $(a \lor b) = \lnot (\lnot a \land \lnot b)$ (De Morgan's law). All we need in order to turn $m_{\{\land, \mathcal{C}, \mathbf{\beta}\}}$ into a disjunction $m_{\{\lor, \mathcal{C}, \mathbf{\beta}\}}$ is another boolean indicator parameter $\alpha \in \{0, 1\}$ signaling whether we want to combine constraints via boolean conjunction ($\alpha = 0$) or disjunction ($\alpha = 1$). Then, the \emph{combination}-aware and \emph{negation}-aware version $m_{\{\alpha, \mathcal{C}, \mathbf{\beta}\}} \colon \mathbb{R}^d \times \mathbb{R}^d \to \{0, 1\}$ is:

\begin{align}
\label{eq:matchRaw}
%m_{\{\alpha, \mathcal{C}, \mathbf{\beta}\}} \colon \mathbb{R}^d \times \mathbb{R}^d &\to \{0, 1\}\\
m_{\{\alpha, \mathcal{C}, \mathbf{\beta}\}} (\mathbf{x}_j, \mathbf{x}_i) &= \alpha \left[ 1 - \prod_k (1 - \hat{c}_k(\mathbf{x}_j, \mathbf{x}_i)) \right] + (1- \alpha) \left[ \prod_k  \hat{c}_k(\mathbf{x}_j, \mathbf{x}_i) \right]
\end{align}

\noindent
While this formulation\footnote{An equivalent formulation can be made with sums over $c_k(\mathbf{x}_j, \mathbf{x}_i)$ and a threshold at $K$ for the conjunction or $1$ for the disjunction. In fact, this is how Equation~\ref{eq:matchRaw} is implemented.} may seem unnecessarily clumsy, it is actually very convenient for parallel implementations on SIMD hardware architectures because it avoids any branching, thus making efficient use of the instruction cache. 

\subsection{Aggregation}

An aggregation $a_{ \{y, \phi \} }(\mathbf{x}_i )$ at transaction $\mathbf{x}_i$ is the result of applying some aggregation function $\phi(\mathcal{M}'_i )$ on feature $y \in \{1, \dots, d\}$ in the set of matching transactions $\mathcal{M}'_i = \{\mathbf{x}_j \mid m_{ \{ \alpha, \mathcal{C}, \beta \} } (\mathbf{x}_j, \mathbf{x}_i) = 1\}$.  The following list shows several simple functions we consider as candidates for $\phi$:

\begin{align}
\phi_1(\mathcal{M}'_i) &= |\mathcal{M}'_i| & \qquad \textit{(count)}\\
\phi_2(\mathcal{M}'_i) &= \sum_{\mathbf{x}_j \in \mathcal{M}'_i} \mathbf{x}_{jy} & \qquad \textit{(sum)}\\
\phi_3(\mathcal{M}'_i) &= \min_{\mathbf{x}_j \in \mathcal{M}'_i} \mathbf{x}_{jy} & \qquad \textit{(min)}\\
\phi_4(\mathcal{M}'_i) &= \max_{\mathbf{x}_j \in \mathcal{M}'_i} \mathbf{x}_{jy} & \qquad \textit{(max)}\\
\phi_5(\mathcal{M}'_i) &= \frac{\sum_{\mathbf{x}_j \in \mathcal{M}'_i} \mathbf{x}_{jy}}{|\mathcal{M}'_i|} & \qquad \textit{(mean)}\\
\phi_6(\mathcal{M}'_i) &= \max_{\mathbf{x}_j \in \mathcal{M}'_i} \mathbf{x}_{jy} - \min_{\mathbf{x}_j \in \mathcal{M}'_i} \mathbf{x}_{jy} & \qquad \textit{(span)}\\
\phi_7(\mathcal{M}'_i) &= \frac{\max_{\mathbf{x}_j \in \mathcal{M}'_i} \mathbf{x}_{jy} - \min_{\mathbf{x}_j \in \mathcal{M}'_i} \mathbf{x}_{jy}}{|\mathcal{M}'_i|} & \qquad \textit{(mean span)}
\end{align}

For reasons of tractability or plain logic, we might not want to consider all matches $\mathcal{M}'_i$ during aggregation but rather restrict aggregation to a subset such as e.g. the 10 transactions that have been issued before\footnote{Assumption 1 states that elements $\mathbf{x}_i \in \mathcal{X}$ are ordered w.r.t. an index set $i \in \{1, \dots, n \}$. Therefore, the notion of before/after can be reduced to comparisons of indices $j < i$ and $j > i$, respectively.} $i$. To that purpose, we extend our definition of an aggregation to include a closed index interval $[u, v] \subset \mathbb{Z}$. Then, an aggregation $a_{ \{y, \phi , u, v\} } \colon \mathcal{X} \to \mathbb{R}$ is defined as 

\begin{align}
a_{ \{y, \phi , u, v\} }(\mathbf{x}_i) = \phi(\mathcal{M}_i)
\end{align}
where
\begin{align}
\mathcal{M}_i = \{i - j \mid m_{ \{ \alpha, \mathcal{C}, \beta \} } (\mathbf{x}_j, \mathbf{x}_i) = 1\} \cap [u, v].
\end{align}

While conceptually simple, this formulation of constraints and aggregation gives rise to a large family of possible features. 

\subsection{Illustration}

In order to illustrate the flexibility emerging from our aggregation concept, we consider a small sample dataset, consisting of  purchase records from a single customer. Table~\ref{tab:dataset} shows the dataset and table~\ref{tab:exaggs} showcases some aggregation features and how they can be expressed canonically with our simple concept.

The design of this concept was motivated by the need to extract aggregations fast and easily. Our goal is to accelerate development cycles in machine learning prototypes. While we could simply exploit the flexibility of a database's query language, we found that this approach is quite slow. Likewise, we could use \texttt{pandas}'s \emph{group-by} objects or \texttt{numpy}'s \emph{where} function, but again we found it to be too slow because we had to call the function millions of times, once for each transaction. At the same time, we noticed that the largest fraction of our aggregations followed the structure illustrated in table~\ref{tab:exaggs} anyway. Therefore, we decided to build a small library that is both specialized in the sense of permitting a fast implementation and yet maximally flexible with respect to the possible aggregations it covers.

In the following section, we discuss briefly the implementation, the library's requirements and the built-in micro query language.

{
\begin{table}
\center
\begin{tabular}{@{}r|lrlrlrlrrr@{}}
\toprule
 & 1 & & 2 & & 3 & & 4 & & 5 & 6\\
\multicolumn{1}{r|}{ID} & \multicolumn{2}{l}{env.} & \multicolumn{2}{l}{auth.} & \multicolumn{2}{l}{country} & \multicolumn{2}{l}{merch.} & \multicolumn{1}{r}{timestamp} & \multicolumn{1}{r}{amount}  \\ \midrule
1   & F2F     & 0   	& PIN  & 0 		&	GBR & 0	  & TSC   & 0 		& 0  		& 44.28    \\
2   & F2F     & 0   	& SIG  & 1 	& 	GBR & 0      & MRS  & 1 	& 45   	& 38.91   \\
3   & ECOM & 1	 	& 3DS & 2		&  LUX  & 1      & AMZ  & 2      & 98   	& 69.99    \\
4   & F2F      & 0		& PIN  & 0		&  GBR & 0      & TSC   & 0      & 109   & 7.54   \\
5   & ECOM  & 1    & None & 3	& USA   & 2      & AVP   & 3      & 129   & 0.99   \\
6   & ECOM  & 1    & None & 3    & USA   & 2      & AVP   & 3      & 130   & 400.00    \\ 
\bottomrule
\end{tabular}
\caption{Example dataset consisting of ordered transactions from a single user. Column \texttt{ID} is not part of the dataset but was added to ease the lookup. An encoding of categorical variables is given to the right of each categorical value.}
\label{tab:dataset}
\end{table}

\begin{table}
\begin{tabular}{p{5cm}p{2cm}p{2cm}p{0.5cm}p{0.7cm}}
\toprule
Description & Target & Constraints & $\alpha$ & $\beta$ \\
\midrule
Amount of previous transaction. & $a_{ \{ 6, \phi_1, -1, -1 \} }$ & & 0 &  $()$\\
Time since previous transaction. & $a_{ \{ 5, \phi_6, -1, 0 \} }$ &  & 0 & $()$\\
Transaction took place in GBR? & $a_{ \{ -, \phi_0, 0, 0 \} }$ & $c_{ \{3, \sigma_{\{0 \}} \} }$ & 0 & $(0,)$\\[1em]
Mean amount in previous 48 hours. & $a_{ \{ 6, \phi_5, -\infty, 0 \} }$ & $c_{ \{5, \delta_{[-48,0]} \} }$ & 0 & $(0,)$\\[3em]
Max amount spent at the same merchant in previous 48 hours. & $a_{ \{ 6, \phi_4, -\infty, 0 \} }$ & $c_{ \{4, \delta_{[0,0]} \} }$, $c_{ \{5, \delta_{[-48,0]} \} }$ & 0 & $(0,0)$\\[2em]
Average time between consecutive transactions at merchant AVP. & $a_{ \{ 5, \phi_7, -\infty, \infty \} }$ & & 0 & () \\ [3em]
Number of transactions with an amount at least as high as the current transaction's amount. &  $a_{ \{ -, \phi_0, -\infty, \infty \} }$ & $c_{ \{6, \delta_{[0, \infty]} \} }$ & 0 & (0,) \\[3em]
Number of transactions with an amount higher than the current transaction's amount. &  $a_{ \{ -, \phi_0, -\infty, \infty \} }$ & $c_{ \{6, \delta_{[-\infty, 0]} \} }$ & 0 & (1,) \\[3em]
Number of previous ECOM transactions that were not issued in GBR or LUX. &  $a_{ \{ -, \phi_0, -\infty, -1 \} }$ & $c_{ \{1, \nu{[1,1]} \} }$, $c_{ \{3, \sigma_{ \{0, 1 \}} \} }$ & 0 & (0,1) \\[3em]
Number of previous transactions at the same merchant whose amount differs from the current amount by at least $\pm 5$. &  $a_{ \{ -, \phi_0, -\infty, -1 \} }$ & $c_{ \{4, \delta{[0,0]} \} }$, $c_{ \{6, \delta_{ [-5, 5] } \} }$ & 0 & (0,1) \\
\bottomrule
\end{tabular}
\caption{Example aggregations for data in table~\ref{tab:dataset}.}
\label{tab:exaggs}
\end{table}


}

%
%\renewcommand{\theFancyVerbLine}{
%  \sffamily\textcolor[rgb]{0.5,0.5,0.5}{\scriptsize\arabic{FancyVerbLine}}}
%
%\begin{minted}[tabsize=4,mathescape,
%               linenos,
%               numbersep=5pt,
%               frame=lines,
%               framesep=2mm]{python}
%
%Target(ay = 0, af = AggMode.SUM, lower=0, upper=0)
%
%\end{minted}
