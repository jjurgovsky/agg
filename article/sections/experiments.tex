\section{Implementation} \label{sec:implementation}
Our feature aggregation library \libname is released as Python source package that can be installed in the usual way via the package manager \texttt{pip}. The core feature aggregation routines in \libname are written in C++. The library contains a parallel CPU implementation and a parallel GPU implementation for CUDA capable devices. The Python bindings are light-weight wrappers around the internal C++ classes and they expose \libname 's functionality to the interpreter. The Python bindings rely on \texttt{numpy}'s \emph{ndarray} as basic data structure for passing data to the extraction routines and receiving back the extracted features.

Regarding aggregation features, a typical use-case is to extract several aggregations from the same transaction sequence. Therefore, we focused on enabling parallelism over a set of aggregations. As our task is embarrassingly parallel, thread synchronization or message passing is not necessary. In the CPU implementation, we launch one thread for each aggregation. In the GPU implementation, we exploit the massive number of threads and further parallelize over transactions in the sequence. Therefore, we assign one thread to each pair of transaction and aggregation.

In \libname we assume transactions to be ordered globally (e.g. by time) and stored as rows in a \texttt{numpy} matrix. 
The default memory layout of \texttt{numpy}'s \emph{ndarray} is row-major order. Also \texttt{pandas} uses a row-major \emph{ndarray} as default buffer. While a column-major layout would be more efficient for the CPU implementation\footnote{Assuming transactions are stored as rows, the two outer loops run along columns. Then each cycle has to skip  $d$ memory elements which results in poor cache usage. Likewise, when writing extracted values to the output buffer, we risk false sharing of cache lines between threads.}, an intermediate copy or in-place transform was found to nullify the performance gain. 

Regarding the input dataset, \libname is designed for two use-cases: (i) Single group transaction sequence or (ii) Multiple groups transaction sequences. In the former, all transactions in the dataset belong to the same group and consequently an aggregation may aggregate over all transactions in the dataset. In the latter case, the dataset consists of transactions associated with different groups and consequently an aggregation may aggregate only over transactions belonging to the same group. The reason for this distinction stems, again, from considerations of computational efficiency. If all transactions belong to the same group, consecutive transactions are coalesced in memory. If transactions from several groups got mixed into a single dataset, consecutive transactions from the same group my reside several rows apart at arbitrary locations in memory. In the latter case, \libname creates a temporary group-contiguous copy because the faster feature extraction usually compensates the additional copy time.

The CUDA implementation makes excessive use of shared memory for caching both transactions and the specifications of aggregations. Compute capability 3.0 is sufficient to run the CUDA routines.

Since the specification of an aggregation in form of Python objects may be a little tedious, \libname comes with its own built-in micro query language\footnote{We construct a suitable parser with the parser generator library \texttt{lark-parser} (\url{https://lark-parser.readthedocs.io/}).}. An example is shown in figure~\ref{lst:exquery}. A list of equivalent queries for the aggregations in table~\ref{tab:exaggs} is given in appendix~\ref{tab:exqueries} and the corresponding BNF grammar in appendix~\ref{lst:querylang-bnf}.

\lstdefinestyle{customc}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  frame=L,
  xleftmargin=6pt,
  language=C,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{green!40!black},
  commentstyle=\itshape\color{purple!40!black},
  identifierstyle=\color{blue},
  stringstyle=\color{orange},
}

\begin{figure}
\center
\begin{lstlisting}[style=customc]
COUNT * FROM -100 TO -1 WHERE ALL
    DELTA(5) IN [-24, 0]
    VALUE(6) > 1.0
\end{lstlisting}
\caption{Example aggregation expressed in micro query language: "Number of transactions issued within the last 24 hours (5) with an amount (6) larger than 1.0. Only the previous 100 transactions are considered for aggregation."}
\label{lst:exquery}
\end{figure}


\section{Evaluation}

The runtime complexity of computing a single feature aggregation is in $\mathcal{O}(m \cdot k^2)$, where $k$ is the length of a group's sequence, and the memory complexity is in $\mathcal{O}(1)$ because eq.~\ref{eq:matchRaw} is computed online in the innermost loop. The runtime is dominated by the group size. However, there are two ways for keeping $k^2$ under control: First, we can restrict the range over which we aggregate by specifying an index interval $[u, v]$. And secondly, we can use the CUDA implementation to effectively turn $k^2$ into $min(t,k) + max(k-t, 0)^2$, where $t$ is the number of threads running concurrently on the device. Which means, as long as $t \geq k$ the effective runtime is in $\mathcal{O}(m \cdot k)$.

\todo{dataset generator, aggregation sampler, report timings as k->inf and a->inf}

Create synthetic data with fake properties for grouped transaction sequences. Rent GPU V100 at Google Cloud. Compare C++/Multi-core C++ / CUDA over various aggregation types. Fast extraction enables real-time feature extraction for time critical applications such as credit card fraud detection (cite) or at least offline feature selection in reasonable time (cite). 


