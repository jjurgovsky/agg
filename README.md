# Install

Install package with

```shell
pip install dist/agg-1.0.tar.gz
```

Since `caggy` comes as source package, it's compiled during setup. 
Therefore make sure to have a working C++ compiler and the python-dev headers. 
If not, just type the following in your shell:

```shell
sudo apt-get install python3-dev
sudo apt-get install g++
```

Also make sure your CUDA toolkit is installed correctly and the runtime libraries are found in the default location `/usr/local/cuda/lib`. 
In case you do not have a CUDA capable device, `caggy` falls back on its CPU implementation.

For getting started, check out the [jupyter notebook](doc.ipynb).



# Feature Aggregation Routines for Feature Extraction from Grouped Sequences (CAGGY)

Transactional data appears in many fields such as payment systems, commerce or the web. Oftentimes such transactions are naturally **ordered in time** and each of them **belongs to a group** such as a user, merchant, account, resource, etc. When developing analytics pipelines on such transactional data, recommendation systems, anomaly detection systems or monitoring systems often rely on simple statistics extracted from a user's purchase history or a resource's usage history. While in many cases any database might be a fine choice, in real-time systems it might be too costly to issue several queries per transaction just to calculate aggregate statistics. 

`caggy` is a small python library with a C++/CUDA backend designed to speed up the extraction of aggregation features from ordered and grouped sequences. It targets developers and researchers who prototype and experiment with machine learning algorithms and who need a simple way to quickly extract various aggregation features from their transactional datasets.

## Overview

Let us consider an example. We assume we have a dataset that consists of purchase transactions given as a 2D numpy array $`X \in \mathbb{R}^{N \times D}`$ in
which rows correspond to ordered transactions and columns correspond to features. We might want to extract a feature such the following:

*"For each transaction in the dataset, sum the amount[1] that was spent in the previous 100 transactions in the same country[2]."* 

In our example, the monetary amount of a transaction resides in column $`1`$ and the country indicator resides in column $`2`$. 

#### 1. Defining an aggregation [ as code ]

The code for creating such an aggregation in `caggy` looks as follows:

```python
import numpy as np
import caggy as ag

# Specify the column, aggregation operator and  
# the index range relative to the current transaction (0 = current transaction)
target = ag.Target(colIdx=1, ag.AggMode.SUM, low=-100, high=-1)

# Specify a relative constraint (DELTA) on column 2 where the difference between 
# the previous transaction's country and the current transaction's country
# is in the interval [0, 0]. I.e. exactly the same.
con = ag.Constraint(ag.ConstraintType.DELTA, 2, low=0, high=0)

# Create an aggregation feature
agg = ag.Aggregation(target, constraints=[con])

# Extract the feature from numpy matrix X
Z = ag.extract(X, [agg])
```

#### 2. Defining an aggregation [ as query ]

Although the direct specification above might be convenient to create aggregations programmatically, it might appear bloated for first time users. 
Luckily, `caggy` also comes with its own SQL like micro-syntax. In this syntax, the same aggregation can be expressed as:

```python
query ="""
SUM 1 FROM -100 TO -1 WHERE ALL
	DELTA(2) == 0
"""
```

And `caggy` creates the corresponding aggregation specification for you:

```python
aggregation = ag.fromQuery(query)
```

The first line of the query defines the target and each following line defines a constraint. Several constraints are combined 
with logical AND  (`ALL`) or logical OR (`ANY`). The next section details the various types of targets and constraints we can express with `caggy`.

## Aggregations

### Constraints

```Python
a = []
for i in range(10):
    a.append(i)
    print(a)
```



#### Preliminaries

```math
x_i, x_j \in \mathbb{R}, \qquad \textit{(value of feature x in transaction i)}
```

```math
d_{ji} = x_j - x_i,  \qquad \textit{(value difference of two transactions j, i)}
```

```math
z_i \in \mathbb{R}, \qquad \textit{(value of aggregation z in transaction i)}
```


#### Constraint Types

```math
\begin{aligned}
& d_{ji} &\in [a_\delta, b_\delta]  & \qquad \textit{(delta interval)}\\
& x_j &\in [a_v, b_v]				& \qquad \textit{(value interval)}\\
& x_j &\in \{a_0, a_1, a_2, \dots \}& \qquad \textit{(value set)}
\end{aligned}
```

Any constraint is simply an indicator function defined on either a delta interval, a value interval or a value set:

```math
\begin{aligned}
\gamma_\delta(x_j, x_i) &= \mathbb{1}_{[a_\delta, b_\delta ]}(x_j - x_i)\\
\gamma_\nu(x_j) &= \mathbb{1}_{[a_v, b_v]}(x_j)\\
\gamma_\sigma(x_j) &= \mathbb{1}_{\{a_0, a_1, a_2, \dots \}}(x_j)\\
\end{aligned}
```

The three constraint types $`\gamma_\delta , \gamma_\nu, \gamma_\sigma`$ possess different semantics: 

- A delta constraint $`\gamma_\delta`$ evaluates to $`1`$ if $`x_j`$ is somewhat close to the value of some pivot $`x_i`$ in the sense $`x_j \in [x_i + a_\delta, x_i + b_\delta]`$

- A value constraint $`\gamma_\nu`$ evaluates to $`1`$ if $`x_j`$ takes values between $`a_\nu \leq x_j \leq b_\nu`$  

- A set constraint $`\gamma_\sigma`$ evaluates to $`1`$ if $`x_j`$ is one of the values in $`\{a_0, a_1, \dots \}`$

  

#### Aggregation

Now we consider a concrete instantiation of several constraints. Each constraint is parameterized by its associated interval, set or, in case of the delta constraint, even some pivot value. Regardless, we simply collapse each constraint's parametrization into the index $k$ and refer to the constraint as $c_k \in \{c_0, c_1, \dots, c_K\}$.

Since a transaction needs to match all $K$ constraints (boolean conjunction) to qualify for feature aggregation, the binary matching function $m_i(x_j)$ is simply a product over indicator functions:
$$
\begin{align}
m_i \colon \mathbb{R} &\to \{0, 1\}\\
x_j &\mapsto \prod_k c_k(x_j)
\end{align}
$$
By keeping the subscript $i$ in $m_i$ we account for the possibility that the matching function might depend on some pivot $x_i$ through the constraints.

An aggregation at transaction $i$ is the result of applying some aggregation function $f(M_i)$ on a set of matching transactions $M_i = \{j \mid m_i(x_j) = 1\}$ . The following list shows several simple functions we could use to aggregate values of the target variable $y$: 
$$
\begin{align}
z_i &= |M_i| & \qquad \textit{(count)}\\
z_i &= \sum_{j \in M_i} y_j & \qquad \textit{(sum)}\\
z_i &= \min_{j \in M_i} y_j & \qquad \textit{(min)}\\
z_i &= \max_{j \in M_i} y_j & \qquad \textit{(max)}\\
z_i &= \frac{\sum_{j \in M_i} y_j}{|M_i|} & \qquad \textit{(mean)}\\
z_i &= \max_{j \in M_i} y_j - \min_{j \in M_i} y_j & \qquad \textit{(span)}\\
z_i &= \frac{\max_{j \in M_i} y_j - \min_{j \in M_i} y_j}{|M_i|} & \qquad \textit{(mean span)}
\end{align}
$$
In order to illustrate the flexibility that emerges from this definition of constraints and targets, we consider a small sample dataset containing records of purchase information from two different customers in 2 different cities.

|      | Customer ID [0] | City ID [1] | Time [2] | Spending [€] [3] |
| ---- | :-------------- | ----------- | -------- | ---------------- |
| 0    | 0               | 2           | 0        | 5                |
| 1    | 1               | 2           | 1        | 1                |
| 2    | 0               | 1           | 2        | 7                |
| 3    | 1               | 1           | 3        | 5                |
| 4    | 1               | 2           | 4        | 2                |
| 5    | 1               | 1           | 5        | 4                |

```python
# Index of the target variable in the dataset
# Aggregation function to apply on the target variable
# The lower endpoint:
#  - we are scanning at most the one hundred previous transactions for matches
# The upper endpoint:
#  - we stop scanning at the current transaction and don't look into the future
target = (3, AggMode.SUM, -100, 0) 

# A list of constraints
# Each constraint is parameterized by 
#  - an index of the constrained variable
#  - the lower delta endpoint
#  - the upper delta endpoint
# This constraint matches all transactions whose value in column 'Spending' does not deviate from the pivot transaction by more than +- 2.
constraints = [(3, -2, 2)]
```



Then, there are Suppose we want to aggregate the values of variable $z$ in a database. The set of For example, suppose we want to keep track of the total amount customer *Timon Berkowitz* has spent in the recent 24 hours. We can formulate this easily via constraints and an aggregation: