//
// Created by Johannes Jurgovsky on 2020-02-26.
//


#include <iostream>
#include <vector>
#include "stdio.h"

#include "../src/types.h"
#include "../src/NumpyMatrix.h"
#include "../src/Aggregation.h"

#include <Eigen/Dense>

template <typename T> using Vector = Eigen::Matrix<T, Eigen::Dynamic, 1>;
template <typename T> using Matrix = Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor>;
template <typename T> using MatrixCM = Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic,Eigen::ColMajor>;


template <typename T> using MapVector = Eigen::Map<Eigen::Matrix<T, Eigen::Dynamic, 1>>;
template <typename T> using MapMatrix = Eigen::Map<Matrix<T>>;
template <typename T> using MapMatrixCM = Eigen::Map<MatrixCM<T>>;

Matrix<tREAL> createMat(tUINT nr = 1000000, tUINT nc = 3, tUINT n_users = 100000, tUINT n_merchs = 1000) {
//    tREAL range = HI-LO;
    srand((unsigned int) time(0));
    Matrix<tREAL> mat = Matrix<tREAL>::Random(nr, nc); // 3x3 Matrix filled with random numbers between (-1,1)
    mat = (mat + Matrix<tREAL>::Constant(nr, nc, 1.)) * 1/2.; // add 1 to the matrix to have values between 0 and 2; multiply with range/2
//    mat = (mat + Matrix<tREAL>::Constant(nr, nc, LO)); //set LO as the lower bound (offset)

    for(int r = 0; r < mat.rows(); r++) {
        mat(r, 0) = (tUINT)(rand() % n_users);
        mat(r, 1) = (tUINT)(rand() % n_merchs);
    }
    return mat;
}

//cudaFuncSetCacheConfig(CudaConstraintTest, cudaFuncCachePreferL1);

using namespace ccfd;
using namespace std::chrono;

int main() {

    // Toy test
//    Matrix<tREAL> X = Matrix<tREAL>::Zero(8,4);
//    X <<    0, 1, 0.1, 0.3,
//            1, 1, 0.1, 3.0,
//            0, 0, 0.2, 9.1,
//            1, 0, 0.3, 9.2,
//            0, 1, 0.4, 0.5,
//            1, 0, 0.5, 7.3,
//            1, 1, 0.6, 4.0,
//            0, 0, 0.7, 4.0;
//
//    std::cout << X << std::endl;

    int nr = 1000000;
    int nu = 10000;
    int nm = 10;
    Matrix<tREAL> X = createMat(nr, 3, nu, nm);

    numpy::NumpyMatrix<tREAL> np_X(X.data(), X.rows(), X.cols(), X.rowStride(), X.colStride(), true);
//    np_X.printData();

    agg::CAggregation a0 = {{2, cuda::AggMode::COUNT, -10, 0}, {{ConstraintType::DELTA_INTERVAL, 1, 0.0, 0.0}, {ConstraintType::DELTA_INTERVAL, 1, -0.3, 0}, {ConstraintType::VALUE_SET, 2, {0.3, 3}}}, ConstraintLogic::AND};
    agg::CAggregation a1 = {{2, cuda::AggMode::SUM, -100, 0}, {{ConstraintType::DELTA_INTERVAL, 1, 0.0, 0.0}}};
    agg::CAggregation a2 = {{2, cuda::AggMode::MEAN, -99, 0}, {{ConstraintType::DELTA_INTERVAL, 1, 0, 0}, {ConstraintType::DELTA_INTERVAL, 2, -5.0, 2.5}}};
    agg::CAggregation a3 = {{2, cuda::AggMode::SPAN, -100, 0}, {{ConstraintType::DELTA_INTERVAL, 2, -5.0, 2.5}}};
    std::vector<agg::CAggregation*> aggs = {&a0, &a1, &a2, &a3};

//    agg::CConstraint c1(ConstraintType::DELTA_INTERVAL, 3, -2.0, 2.0);
//    agg::CConstraint c2(ConstraintType::DELTA_INTERVAL, 4, -3.0, 3.0);

//    agg::CAggregation agg({2, cuda::AggMode::COUNT, -10, 0}, {c1, c2});

//    CUDAAggregation& cagg = agg.getCUDAAggregation(Memory::HOST);

//    for(int c = 0; c < cagg.C; c++) {
//        cagg.constraints[c].print();
//    }


    numpy::NumpyMatrix<tREAL> Z(nr, aggs.size(), 0.0);

    agg::CAggregator aggregator(np_X, 0);
    aggregator.extract(aggs, Z, 0.0, true, false, true, true);

//    agg::extractFromSequence(np_X, aggs, Z, 0.0, false, true, true, true);

//    Z.printData();

    for(tUINT r = Z.rows()-1; r > Z.rows()-10; r--) {
        for(tUINT c = 0; c < Z.cols(); c++) {
            std::cout << Z.get(r,c) << " ";
        }
        std::cout << std::endl;
    }
//
//    test();

//    CTarget tar {0, AggMode::MIN, -100, 0};
//    CConstraint co1(ConstraintType::DELTA_INTERVAL, 2, -1.0, 1.0);
//    std::vector<tREAL> vset = {19.0, -1.0, 23.0, 0.0, -4.888};
//    CConstraint co2(ConstraintType::VALUE_SET, 2, vset);
//    std::vector<CConstraint> colist = {co1, co2};
//    CAggregation agg {tar, colist, ConstraintLogic::OR};
//
//
//    CTarget tar2 {2, AggMode::MEAN, -101, 2};
//    CConstraint co21{ConstraintType::VALUE_INTERVAL, 0, 50, 100};
//    CConstraint co22{ConstraintType::VALUE_SET, 1, vset};
//    std::vector<CConstraint> colist2 = {co21, co22};
//    CAggregation agg2 {tar2, colist2, ConstraintLogic::OR};
//
//    std::vector<CAggregation> aggs = {agg, agg2};
//
//    int A = aggs.size();
//    CUDAAggregation* d_aggs = copyAggregationsToDevice(aggs);
//
//    int N = 33;
//    int BSX = 16;
//    int BSY = A;
//    int GSX = N/BSX + (N % BSX != 0);
//    int GSY = 1;
//    dim3 blockDim (BSX, BSY);
//    dim3 gridDim (GSX, GSY);
//
//    int n = 4;
//    int n_shared = 10000;
//    float* h_ptr = new float[n];
//    float* d_ptr;
//    cudaMalloc(&d_ptr, sizeof(float) * n);
//
//    CudaTargetTest<<<1,1, n_shared>>>(tar.getDevicePtr(), d_ptr);
//    cudaDeviceSynchronize();
//
//    CudaConstraintTest<<<1,1, n_shared>>>(co1.getDevicePtr(), d_ptr);
//    cudaDeviceSynchronize();
//
//    CudaConstraintTest<<<1,1, n_shared>>>(co2.getDevicePtr(), d_ptr);
//    cudaDeviceSynchronize();
//
//    CudaAggregationTest<<<1,1, n_shared>>>(agg.getDevicePtr(), d_ptr);
//    cudaDeviceSynchronize();
//
//    printf("\nCuda Aggregation Array Test\n\n");
//    CudaAggregationArrayTest<<<1, 1, n_shared>>>(d_aggs, A, d_ptr);
//    cudaDeviceSynchronize();
//
//    deleteAggregationsFromDevice(d_aggs);
//
//    cudaMemcpy(h_ptr, d_ptr, sizeof(float) * n, cudaMemcpyDeviceToHost);
//    std::cout << h_ptr[0] << " " << h_ptr[1] << " " << h_ptr[2] << " " << h_ptr[3] << std::endl;
//
//    cuda_check_error(cudaPeekAtLastError());


//    CConstraint co1(ConstraintType::DELTA_INTERVAL, 0, -1.0, 1.0);
//    CConstraint co2(ConstraintType::VALUE_INTERVAL, 1, -100000.0, 0.0);
//    CConstraint co3(ConstraintType::VALUE_SET, 2, {{9.0, 293., 1.0 }});
//
//    CTarget tar {1, 7, -100, 0};
//
//    std::vector<CConstraint> colist = {co1, co2, co3};
//
//
//    std::cout << std::endl << sizeof(CTarget) << " " << sizeof(CConstraint) << std::endl;
//
//    tar.print();
//    for(auto& co : colist) {
//        co.print();
//    }

//    CUDAAggregation agg = initCUDAAggregation(tar, colist);

//    for(int c = 0; c < agg.C; c++) {
//        printf("ct: %d ci: %d cl: %.1f cu: %.1f\n", agg.ct[c], agg.ci[c], agg.cl[c], agg.cu[c]);
//        printf("vset: {");
//        for(int vi = 0; vi < agg.cvsn[c]; vi++) {
//            printf("%.1f, ", agg.cvs[c][vi]);
//        }
//        printf("}\n");
//    }


    return 0;
}