import setuptools
#from distutils.core import setup
#from distutils.extension import Extension
from setuptools import setup, Extension

from Cython.Distutils import build_ext
from Cython.Build import cythonize

from distutils.command.install import install as DistutilsInstall

import os
import subprocess
import numpy

link_libraries = ["stdc++"]
hasNVCC = 'nvcc' in subprocess.run(['which', 'nvcc'], stdout=subprocess.PIPE).stdout.decode('utf-8')
if hasNVCC:
    link_libraries += ["cuda", "cudart"]


# class MyInstall(DistutilsInstall):
#     def run(self):
#         print("building cuda library")
#         os.system("make -B")
#         print("starting install")
#         DistutilsInstall.run(self)

class mybuild(build_ext):
    def run(self):
        print("Building cuda library 'libagg'")
        os.system("make -B")
        build_ext.run(self)

setup(
    name = "agg",
    version = '1.0',
    description = "Feature Aggregation Routines for Feature Extraction from Grouped Sequences.",
    author='Johannes Jurgovsky',
    author_email="johannes.jurgovsky@googlemail.com",
    classifiers=["Feature Extraction :: Aggregation :: Grouped Sequences :: Python :: 3 :: C++ :: CUDA"],
    cmdclass = {'build_ext': mybuild},#, 'install': MyInstall},
    setup_requires=['wheel', 'numpy'],
    install_requires=['lark-parser'],
    zip_safe=False,
    ext_modules=
        [
            Extension("agg",
                      sources=["cyth/agg.cython.cpp"],
                      language="C++",
                      library_dirs=["/usr/local/cuda/lib", "/usr/local/cuda/lib64"],
                      libraries=link_libraries,#["cuda", "cudart"],
                      include_dirs=[numpy.get_include(), "src/"],
                      extra_compile_args=["-std=c++11", "-O3"],
                      extra_objects=["./build/libagg.a"]
                      )
        ]
    # compiler_directives={'language_level': "3"},
    # force=True
)



#LD_LIBRARY_PATH must contain ... cuda/lib for libcuda.so and libcudart.so to be found
#LIBRARY_PATH must contain ... cuda/lib for libcuda.so and libcudart.so to be found

# > python3 setup.py install


# setup.py needs to have
# link libraries must have stdc++ lib
# install_requires=['lark-parser']
# library_dirs=["/usr/local/cuda/lib64"]

# user needs to have:
# sudo apt-get install python3-dev
# sudo apt-get install g++

# Create source distribution tarball: make sdist
# Install from tarball: pip install dist/agg-1.0.tar.gz




# TODO:
# add -INF, INF to grammar for FROM x TO x







# setup(
#     name = "agg",
#     version = '1.0',
#     description = "Feature Aggregation Routines for Feature Extraction from Grouped Sequences.",
#     author='Johannes Jurgovsky',
#     author_email="johannes.jurgovsky@googlemail.com",
#     classifiers=["Feature Extraction :: Aggregation :: Python :: 3 :: CUDA :: C++"],
#     cmdclass = {'build_ext': mybuild, 'install': MyInstall},
#     setup_requires=['wheel', 'cython', 'numpy', 'lark-parser'],
#     zip_safe=False,
#     ext_modules=cythonize(
#         [
#             Extension("agg",
#                       sources=["cyth/agg.pyx"],
#                       language="C++",
#                       #library_dirs=["/usr/local/cuda/lib"],
#                       libraries=link_libraries,#["cuda", "cudart"],
#                       include_dirs=[numpy.get_include()],
#                       extra_compile_args=["-std=c++11", "-O3"],
#                       extra_objects=["./build/libagg.a"]
#                       )
#         ],
#         compiler_directives={'language_level': "3"},
#         force=True
#     )
# )
