//
// Created by Johannes Jurgovsky on 2020-01-21.
//

#ifndef CCFD_MATRIX_TYPES_H
#define CCFD_MATRIX_TYPES_H

#define HASH_SIZE 128


#include <iostream>
#include <cstring>
#include <assert.h>

#include "types.h"


enum BufferTranspose {
    COL_TO_ROW,
    ROW_TO_COL
};

enum StorageOrder {
    COL_MAJOR,
    ROW_MAJOR
};


//// In-place matrix transpose for changing storage order
//// Code is from post http://tinyurl.com/j79j445
//// Non-square matrix transpose of matrix of size r x c and base address A
//template <typename T>
//void transpose(T *A, size_t r, size_t c)
//{
//    size_t size = r*c - 1;
//    T t; // holds element to be replaced, eventually
//    // becomes next element to move
//    size_t next; // location of 't' to be moved
//    size_t cycleBegin; // holds start of cycle
//
//    std::bitset<HASH_SIZE> b; // hash to mark moved elements
//
//    b.reset();
//    b[0] = b[size] = 1;
//    size_t i = 1; // Note that A[0] and A[size-1] won't move
//    while (i < size)
//    {
//        cycleBegin = i;
//        t = A[i];
//        do
//        {
//            // Input matrix [r x c]
//            // Output matrix 1
//            // i_new = (i*r)%(N-1)
//            next = (i*r)%size;
//            std::swap(A[next], t);
//            b[i] = 1;
//            i = next;
//
//        } while (i != cycleBegin);
//
//        // Get Next Move (what about querying
//        // random location?)
//        for (i = 1; i < size && b[i]; i++)
//            ;
//    }
//}



namespace ccfd {
    namespace numpy {

        template<typename T>
        class NumpyMatrix {
        private:
            T *m_data = nullptr;
            long m_rows = -1;
            long m_cols = -1;
            long m_rstride = -1;
            long m_cstride = -1;
            StorageOrder m_storageOrder = StorageOrder::ROW_MAJOR;
            bool m_is_buffer = false;

            void init(long rows, long cols, long rstride, long cstride, StorageOrder storageOrder) {
                m_rows = rows;
                m_cols = cols;
                m_rstride = rstride;
                m_cstride = cstride;
                m_storageOrder = storageOrder;
            }

        public:
            NumpyMatrix() = default;

            ~NumpyMatrix() {
                if(m_data == nullptr) {
                    return;
                }
                if(!m_is_buffer) {
                    delete[] m_data;
                }
            };

            NumpyMatrix(long rows, long cols, T fill_value = 0, StorageOrder storageOrder = ROW_MAJOR) {
                if(storageOrder == ROW_MAJOR) {
                    init(rows, cols, cols, 1, ROW_MAJOR);
                } else {
                    init(rows, cols, 1, rows, COL_MAJOR);
                }

                m_data = new T[m_rows * m_cols];
                m_is_buffer = false;
                for(size_t i = 0; i < size(); i++) {
                    m_data[i] = fill_value;
                }
            }

            NumpyMatrix(T *data, long *shape, long *stride, bool row_major) :
                m_data(data),
                m_rows(shape[0]),
                m_cols(shape[1]),
                m_rstride(stride[0] / sizeof(T)),
                m_cstride(stride[1] / sizeof(T)),
                m_storageOrder(row_major ? StorageOrder::ROW_MAJOR : StorageOrder::COL_MAJOR),
                m_is_buffer(true)
            {};

            NumpyMatrix(T *data, long rows, long cols, long rstride, long cstride, bool row_major) :
                m_data(data),
                m_rows(rows),
                m_cols(cols),
                m_rstride(rstride),
                m_cstride(cstride),
                m_storageOrder(row_major ? StorageOrder::ROW_MAJOR : StorageOrder::COL_MAJOR),
                m_is_buffer(true)
            {};


            NumpyMatrix(const NumpyMatrix& other) // copy constructor
            {
                init(other.m_rows, other.m_cols, other.m_rstride, other.m_cstride, other.m_storageOrder);
                m_is_buffer = other.m_is_buffer;
                if(other.m_is_buffer) {
                    m_data = other.m_data;
                } else {
                    m_data = new T[m_rows * m_cols];
                    std::memcpy(m_data, other.m_data, sizeof(T) * other.size());
                }
            }

            NumpyMatrix(NumpyMatrix&& other) noexcept // move constructor
            : m_data(other.m_data)
            {
                other.m_data = nullptr;
                init(other.m_rows, other.m_cols, other.m_rstride, other.m_cstride, other.m_storageOrder);
                m_is_buffer = other.m_is_buffer;
            }

            NumpyMatrix& operator=(const NumpyMatrix& other) // copy assignment
            {
                if(&other == this) {
                    return *this;
                }
                if(!m_is_buffer) {
                    delete[] m_data;
                }

                init(other.m_rows, other.m_cols, other.m_rstride, other.m_cstride, other.m_storageOrder);
                m_is_buffer = other.m_is_buffer;
                if(!other.m_is_buffer) {
                    m_data = new T[other.size()];
                    memcpy(m_data, other.m_data, sizeof(T) * other.size());
                } else {
                    m_data = other.m_data;
                }
                return *this;
            }

            NumpyMatrix& operator=(NumpyMatrix&& other) noexcept // move assignment
            {
                if(&other == this) {
                    return *this;
                }
                if(!m_is_buffer) {
                    delete[] m_data;
                }
                init(other.m_rows, other.m_cols, other.m_rstride, other.m_cstride, other.m_storageOrder);
                m_is_buffer = other.m_is_buffer;

                m_data = other.m_data;
                other.m_data = nullptr;
                return *this;
            }

            T* data() const {
                return m_data;
            }

            size_t rows() const {
                return m_rows;
            }

            size_t cols() const {
                return m_cols;
            }

            size_t size() const {
                return m_rows * m_cols;
            }

            size_t rstride() const {
                return m_rstride;
            }

            size_t cstride() const {
                return m_cstride;
            }

            bool isRowMajorOrder() const {
                return m_storageOrder == StorageOrder::ROW_MAJOR;
            }

            inline T get(size_t r, size_t c) const {
                assert(r >= 0 && r < m_rows);
                assert(c >= 0 && c < m_cols);
                return m_data[r * m_rstride + c * m_cstride];
            }

            inline void set(size_t r, size_t c, T value) {
                assert(r >= 0 && r < m_rows);
                assert(c >= 0 && c < m_cols);
                m_data[r * m_rstride + c * m_cstride] = value;
            }

//            void transposeBuffer(BufferTranspose bufferTranspose) {
//                if(bufferTranspose == COL_TO_ROW) {
//                    transpose(m_data, m_cols, m_rows);
//                } else {
//                    transpose(m_data, m_rows, m_cols);
//                }
//            }

            void print() {
                printf("NumpyMatrix<T> | shape (%ld, %ld) stride (%ld, %ld) CContiguous (%s)\n", m_rows, m_cols, m_rstride, m_cstride, m_storageOrder ? "true" : "false");
            }

            void printData() const {
                for(tUINT r = 0; r < m_rows; r++) {
                    for(tUINT c = 0; c < m_cols; c++) {
                        printf("%.5g\t", get(r, c));
                    }
                    printf("\n");
                }
            }

            void printRaw() {
                for(tUINT c = 0; c < size(); c++) {
                    printf("%.5g\t", m_data[c]);
                }
                printf("\n");
            }
        };
    }
}







////// OLD STUFF

//        template<typename T, int ndim>
//        class NumpyArray {
//        private:
//            T *m_data;
//            long *m_shape;
//            long *m_stride;
//            long m_ndim;
//            bool m_c_contiguous;
//
//        public:
//            NumpyArray() = default;
//
//            ~NumpyArray() = default;
//
//            NumpyArray(T *data, long *shape, long *stride, bool c_contiguous) : m_data(data), m_shape(shape), m_stride(stride), m_c_contiguous(c_contiguous) {};
//
//            T* data() {
//                return this->m_data;
//            }
//
//            long shape(long axis) {
//                return this->m_shape[axis];
//            }
//
//            long stride(long axis) {
//                return this->m_stride[axis];
//            }
//
//            long rows() {
//                return m_shape[0];
//            }
//
//            long cols() {
//                if(ndim == 2) {
//                    return m_shape[1];
//                } else {
//                    return 1;
//                }
//            }
//
//            void printme() {
//                if(ndim == 2) {
//                    std::cout << "NumpyArray<T> | ndim=" << ndim << " c_contiguous=" << std::to_string(m_c_contiguous) << " shape=(" << m_shape[0] << ", " << m_shape[1] << ")" << " stride=(" << m_stride[0] << ", " << m_stride[1] << ")"<< std::endl;
//                }
//            }
//
//            MapMatrix<T> getEigenMatrix() const {
//                assert(ndim == 2);
//                MapMatrix<T> mat(m_data, m_shape[0], m_shape[1]);
//                return mat;
//            }
//
//            MapVector<T> getEigenVector() const {
//                assert(ndim == 1);
//                MapVector<T> vec(m_data, m_shape[0]);
//                return vec;
//            }
//
//            void addOne(NumpyArray<T, 2> arr) {
//                for(int i = 0; i < arr.shape(0); i++) {
//                    arr.data()[i] += 1;
//                }
//                arr.printme();
//
//                std::cout << arr.getEigenMatrix() << std::endl;
//            }
//        };



//template <typename T> using NumpyMatrix = ccfd::numpy::NumpyArray<T,2>;
//template <typename T> using NumpyVector = ccfd::numpy::NumpyArray<T,1>;

//template <typename T> void passMatrix(NumpyMatrix<T> arr) {
//    MapMatrix<T> mat = arr.getEigenMatrix();
//    std::cout << mat << std::endl;
//    std::cout << "adding +1..." << std::endl;
//    for(int r = 0; r < mat.rows(); r++) {
//        for(int c = 0; c < mat.cols(); c++) {
//            mat(r,c) += 1;
//        }
//    }
//    std::cout << mat << std::endl;
//}
//
//template <typename T> void passVector(NumpyVector<T> arr) {
//    MapVector<T> vec = arr.getEigenVector();
//    std::cout << vec << std::endl;
//    std::cout << "adding +1..." << std::endl;
//    for(int r = 0; r < vec.size(); r++) {
//        vec(r) += 1;
//    }
//    std::cout << vec << std::endl;
//}
//



#endif //CCFD_MATRIX_TYPES_H
