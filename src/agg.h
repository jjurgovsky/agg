//
// Created by Johannes Jurgovsky on 2020-02-13.
//
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION


#ifndef CCFD_CUDA_AGG_H
#define CCFD_CUDA_AGG_H

#include <iostream>
#include <exception>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <thread>
#include <assert.h>

#include "types.h"

#ifdef __CUDACC__
#define CUDA_HOST_DEVICE __host__ __device__
#define CUDA_HOST __host__
#define CUDA_DEVICE __device__
#define CUDA_INLINE __forceinline__
#else
#define CUDA_HOST_DEVICE
#define CUDA_HOST
#define CUDA_DEVICE
#define CUDA_INLINE
#endif


class CUDANotAvailableException: public std::exception
{
    const char* what() const noexcept override
    {
        return "Function not implemented. Make sure CUDA Toolkit and CUDA compiler 'nvcc' are installed correctly.";
    }
};

bool gpu_available();

const tREAL EPS = 0.000001;



namespace ccfd {
    namespace cuda {

        typedef enum Memory {
            HOST,
            DEVICE
        } Memory;

        typedef enum AggMode {
            SUM,
            MEAN,
            COUNT,
            MIN,
            MAX,
            SPAN,
            MEAN_SPAN
        } AggMode;

        typedef enum ConstraintType {
            DELTA_INTERVAL,
            VALUE_INTERVAL,
            VALUE_SET
        } ConstraintType;

        typedef enum ConstraintLogic {
            AND,
            OR
        } ConstraintLogic;

        struct CUDAGroupedData {
            bool onDevice = false;
            tUINT nr = 0;
            tUINT nc = 0;
            tUINT ng = 0;
            tUINT* ki = nullptr;
            tUINT* gs = nullptr;
            tUINT* gn = nullptr;
            tREAL* GX = nullptr;

            CUDA_HOST_DEVICE void print() const {
                printf("ki \t gs \t gn\n");
                for(tUINT i = 0; i < nr; i++) {
                    printf("%lu \t %lu \t %lu\n", ki[i], gs[i], gn[i]);
                }
            }
        };

        struct CUDATarget {
            int ay = 0;
            AggMode af = AggMode::COUNT;
            int al = 0;
            int au = 0;

            CUDA_HOST_DEVICE void print() {
                printf("__device__ CUDATarget::print() : %d %d %d %d\n", ay, (int)af, al, au);
            }
        };


        struct CUDAConstraint {
            ConstraintType ct = ConstraintType::DELTA_INTERVAL;
            int ci = 0;
            tREAL cl = 0;
            tREAL cu = 0;
            int neg = 0;
            tREAL* vset = nullptr;
            int vset_n = 0;

            CUDA_HOST_DEVICE void print() {
                printf("__device__ CUDAConstraint::print() : ct = %d ci = %d neg = %d cl = %.1f cu = %.1f\n", (int)ct, ci, neg, cl, cu);
                printf("vset: {");
                for(int vi = 0; vi < vset_n; vi++) {
                    printf("%.1f, ", vset[vi]);
                }
                printf("}\n");
            }

            CUDA_HOST_DEVICE CUDA_INLINE int isMatch(const tREAL* row_j, const tREAL* row_i) {
                bool match = 0;
                tREAL vj = row_j[ci];
                switch(ct) {
                    case ConstraintType::DELTA_INTERVAL: {
                        tREAL dji = vj - row_i[ci];
                        match = (cl <= dji) && (dji <= cu);
                        break;
                    }
                    case ConstraintType::VALUE_INTERVAL: {
                        match = (cl <= vj) && (vj <= cu);
                        break;
                    }
                    case ConstraintType::VALUE_SET: {
                        int i = 0;
                        for(i = 0; (i < (vset_n - 1)) && vset[i] < vj; i++) {;}
                        match = vset[i] == vj;
                        break;
                    }
                    default:
                        ;
                }
                return (int)match;
            }
        };


        struct CUDAAggregation {
            int C = 0;
            CUDATarget target;
            ConstraintLogic logic = ConstraintLogic::AND;
            CUDAConstraint* constraints = nullptr;

            CUDA_HOST_DEVICE CUDA_INLINE void intersection(int& a0, int& a1, int& b0, int& b1, int& z0, int& z1) {
                if(b1 < a0 || b0 > a1) {
                    z0 = 1;   // undefined interval: empty set, force immediate exit from for loop
                    z1 = -1;  // undefined interval: empty set, force immediate exit from for loop
                } else {
                    // at least one element in integer interval
                    z0 = b0 >= a0 ? b0 : a0; // z0 = max(a0, b0)
                    z1 = b1 <= a1 ? b1 : a1; // z1 = min(a1, b1)
                }
            }

            CUDA_HOST_DEVICE CUDA_INLINE bool isMatch(const tREAL* row_j, const tREAL* row_i) {

                // holy, that looks unnecessarily complicated. Here is what's going on:
                // Constraints are boolean variables that evaluate to 0|1 -> mac
                // Any constraint may be negated -> neg
                // All constraints are combined either via boolean AND or boolean OR
                // e.g. (!c0 && c1 && !c2 && ...) or (!c0 || c1 || !c2 || ...)
                // To avoid if-then-else branches (cuda parallelism is SIMD),
                // we use simple arithmetic to get the same result.

                int matches = 0;
                int mac = 0;
                int neg = 0;
                int logic_thr = logic == ConstraintLogic::AND ? C : int(C > 0);  // always return true if no constraints

                for(int c = 0; c < C; c++) {
                    neg = constraints[c].neg;
                    mac = constraints[c].isMatch(row_j, row_i);
                    matches += neg * (1 - mac) + (1 - neg) * mac;
                }
                return matches >= logic_thr;
            }

            CUDA_HOST_DEVICE CUDA_INLINE void update(tREAL& v, tREAL& s_cnt, tREAL& s_sum, tREAL& s_min, tREAL& s_max) {
                s_sum += v;
                s_cnt += 1.0f;
                if(s_cnt == 1.0f) {
                    s_min = v;
                    s_max = v;
                } else {
                    s_min = v < s_min ? v : s_min;
                    s_max = v > s_max ? v : s_max;
                }
            }

            CUDA_HOST_DEVICE CUDA_INLINE tREAL getValue(tREAL& s_sum, tREAL& s_cnt, tREAL& s_min, tREAL& s_max, tREAL& default_value) {
                if(s_cnt <= EPS) {
                    return default_value;
                }
                tREAL v;
                switch(target.af) {
                    case AggMode::SUM:
                        v = s_sum; break;
                    case AggMode::MEAN:
                        v = s_cnt > EPS ? s_sum / s_cnt : default_value; break;
                    case AggMode::COUNT:
                        v = s_cnt; break;
                    case AggMode::MIN:
                        v = s_min; break;
                    case AggMode::MAX:
                        v = s_max; break;
                    case AggMode::SPAN:
                        v = s_max - s_min; break;
                    case AggMode::MEAN_SPAN:
                        v = s_cnt > EPS ? (s_max - s_min) / s_cnt : default_value; break;
                    default:
                        v = default_value;
                }
                return v;
            }

            CUDA_HOST_DEVICE void print() {
                printf("__device__ CUDAAggregation::print() : C = %d logic = %s\n", C, logic == ConstraintLogic::AND ? "AND" : "OR");
                target.print();
                for(int c = 0; c < C; c++) {
                    constraints[c].print();
                }
            }
        };

        template<typename T> void allocCopyToDevice(T **d, const T *h, size_t n_bytes);
//        template<typename T> void freeFromDevice(T *d); //wtf? linker error with template function

        CUDAGroupedData copyGroupedDataToDevice(CUDAGroupedData& gdata);
        void deleteGroupedDataFromDevice(CUDAGroupedData& gdata);

        CUDAAggregation* copyAggregationsToDevice(std::vector<CUDAAggregation>& aggs);
        void deleteAggregationsFromDevice(CUDAAggregation *d_aggs);

        void copyTargetToDevice(CUDATarget** dst, CUDATarget* src);
        void copyConstraintToDevice(CUDAConstraint** dst, CUDAConstraint* src);
        void copyAggregationToDevice(CUDAAggregation** dst, CUDAAggregation* src);

        void deleteAggregationFromDevice(CUDAAggregation* d);
        void deleteConstraintFromDevice(CUDAConstraint* d);
        void deleteTargetFromDevice(CUDATarget* d);

        void freeFromDevice(tREAL* d);

        void extract(CUDAGroupedData& gdata, std::vector<CUDAAggregation>& aggs, tREAL * h_Z, tREAL default_value = 0.0, bool fVerbose = false);
    }
}


#endif //CCFD_CUDA_AGG_H