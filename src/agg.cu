//
// Created by Johannes Jurgovsky on 2020-02-13.
//

//#include <iostream>
//#include <driver_types.h>
#include "agg.h"

// COMPILE-TIME CHECK FOR CUDA COMPILER. IF NOT COMPILED WITH NVCC -> DEACTIVATE ALL CUDA RELATED CODE
#ifdef __CUDACC__
#define NVCC_COMPILER
#endif

#ifdef NVCC_COMPILER
#define cuda_check_error(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
    if (code != cudaSuccess)
    {
        fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort) exit(code);
    }
}
#endif

// RUN-TIME CHECK FOR AVAILABLE CUDA DEVICE.
bool gpu_available() {
#ifdef NVCC_COMPILER
    int max_devices = 0;
    cudaDeviceProp props;
    cuda_check_error(cudaGetDeviceCount(&max_devices))
    for(int i = 0; i < max_devices; i++) {
        cuda_check_error(cudaGetDeviceProperties(&props, i));
        if(props.major >= 3) {
            return true;
        }
    }
    return false;
#else
    return false;
#endif
}

const char* cuda_err_str = "WARNING: Extension was compiled without CUDA. Make sure CUDA Toolkit and compiler are installed correctly. Fallback to CPU implementation.\n";



namespace ccfd {
    namespace cuda {

        template<typename T>
        void allocCopyToDevice(T **d, const T *h, size_t n_bytes) {
#ifdef NVCC_COMPILER
            cuda_check_error(cudaMalloc(d, n_bytes));
            cuda_check_error(cudaMemcpy(*d, h, n_bytes, cudaMemcpyHostToDevice));
#else
//            printf("%s", cuda_err_str);
            throw CUDANotAvailableException();
#endif
        }

        template<typename T>
        void freeFromDevice(T *d) {
#ifdef NVCC_COMPILER
            cuda_check_error(cudaFree(d));
#else
            throw CUDANotAvailableException();
#endif
        }

        // wtf? Can't link to lib if that's a template function
        void freeFromDevice(tUINT* d) {
#ifdef NVCC_COMPILER
            cuda_check_error(cudaFree(d));
#else
            throw CUDANotAvailableException();
#endif
        }

        void freeFromDevice(tREAL* d) {
#ifdef NVCC_COMPILER
            cuda_check_error(cudaFree(d));
#else
            throw CUDANotAvailableException();
#endif
        }

        void deleteTargetFromDevice(CUDATarget* d) {
#ifdef NVCC_COMPILER
            cuda_check_error(cudaFree(d));
#else
            throw CUDANotAvailableException();
#endif
        }

        void deleteConstraintFromDevice(CUDAConstraint* d) {
#ifdef NVCC_COMPILER
            cuda_check_error(cudaFree(d));
#else
            throw CUDANotAvailableException();
#endif
        }

        void deleteAggregationFromDevice(CUDAAggregation* d) {
#ifdef NVCC_COMPILER
            cuda_check_error(cudaFree(d));
#else
            throw CUDANotAvailableException();
#endif
        }

        CUDAGroupedData copyGroupedDataToDevice(CUDAGroupedData& gdata) {
            CUDAGroupedData d_gdata;
#ifdef NVCC_COMPILER
            //CGroupedData is a regular host object, only its pointers point to device memory. NEVER de-reference them on host.
//            CUDAGroupedData d_gdata;
            if (!gdata.onDevice) {
                d_gdata.onDevice = true;
                d_gdata.nr = gdata.nr;
                d_gdata.nc = gdata.nc;
                d_gdata.ng = gdata.ng;
                allocCopyToDevice(&d_gdata.ki, gdata.ki, gdata.nr * sizeof(tUINT));
                allocCopyToDevice(&d_gdata.gs, gdata.gs, gdata.nr * sizeof(tUINT));
                allocCopyToDevice(&d_gdata.gn, gdata.gn, gdata.nr * sizeof(tUINT));
                allocCopyToDevice(&d_gdata.GX, gdata.GX, gdata.nr * gdata.nc * sizeof(tREAL));
            } else {
                d_gdata = gdata;
            }
            return d_gdata;
#else
            throw CUDANotAvailableException();
#endif
        }

        void deleteGroupedDataFromDevice(CUDAGroupedData &gdata) {
#ifdef NVCC_COMPILER
            if (gdata.onDevice) {
                freeFromDevice(gdata.ki);
                freeFromDevice(gdata.gs);
                freeFromDevice(gdata.gn);
                freeFromDevice(gdata.GX);
                gdata.onDevice = false;
                gdata.nr = 0;
                gdata.gs = nullptr;
                gdata.gn = nullptr;
                gdata.GX = nullptr;
            }
#else
            throw CUDANotAvailableException();
#endif
        }

        CUDAAggregation* copyAggregationsToDevice(std::vector<CUDAAggregation>& aggs) {
            CUDAAggregation *d_aggs = nullptr;
#ifdef NVCC_COMPILER
            int A = aggs.size();
            allocCopyToDevice<CUDAAggregation>(&d_aggs, aggs.data(), A * sizeof(CUDAAggregation));
            return d_aggs;
#else
            throw CUDANotAvailableException();
#endif
        }

        void deleteAggregationsFromDevice(CUDAAggregation *d_aggs) {
#ifdef NVCC_COMPILER
            freeFromDevice(d_aggs);
#else
            throw CUDANotAvailableException();
#endif
        }

        void copyTargetToDevice(CUDATarget** dst, CUDATarget* src) {
#ifdef NVCC_COMPILER
            allocCopyToDevice(dst, src, sizeof(CUDATarget));
#else
            throw CUDANotAvailableException();
#endif
        }

        void copyConstraintToDevice(CUDAConstraint** dst, CUDAConstraint* src) {
#ifdef NVCC_COMPILER
            allocCopyToDevice<tREAL>(&(src->vset), src->vset, src->vset_n * sizeof(tREAL));
            allocCopyToDevice<CUDAConstraint>(dst, src, sizeof(CUDAConstraint));
#else
            throw CUDANotAvailableException();
#endif
        }

        void copyAggregationToDevice(CUDAAggregation** dst, CUDAAggregation* src) {
#ifdef NVCC_COMPILER
            allocCopyToDevice<CUDAConstraint>(&(src->constraints), src->constraints, src->C * sizeof(CUDAConstraint));
            allocCopyToDevice<CUDAAggregation>(dst, src, sizeof(CUDAAggregation));
#else
            throw CUDANotAvailableException();
#endif
        }

        void copyZToHost(tREAL *d_z, tREAL *h_z, size_t n) {
#ifdef NVCC_COMPILER
            cuda_check_error(cudaMemcpy(h_z, d_z, n * sizeof(tREAL), cudaMemcpyDeviceToHost));
#else
            throw CUDANotAvailableException();
#endif
        }

#ifdef NVCC_COMPILER

        extern __shared__ tREAL buffer[];


        // pass pointer to Aggregations in global memory and pointer to aggregations in shared memory ...or whatever memory.
        __device__ void copyAggregationsToShared(const CUDAAggregation* g_aggs, CUDAAggregation* s_aggs, int A) {

            size_t a = threadIdx.y;

            // Assert function called with threadIdx.y \in [0, A)
            if(threadIdx.x == 0 && a < A) {

                // Each of A threads copies one aggregation to shared memory
                s_aggs[a] = g_aggs[a];
                __syncthreads();

                // Arrange all constraints contiguously inside buffer.
                // Start at memory location right after end of last aggregation
                CUDAConstraint *_tmp_const_ptr = (CUDAConstraint *) &s_aggs[A];

                // Each thread (a) needs to know at which address his constraints need to be placed. i.e. how many constraints come before.
                int cumsum = 0;
                for (int _pa = 0; _pa < a; _pa++) {
                    cumsum += s_aggs[_pa].C;
                }
                CUDAConstraint *constraints = &_tmp_const_ptr[cumsum];

                // Copy constraints of the a-th aggregation by thread a
                for (int c = 0; c < s_aggs[a].C; c++) {
                    constraints[c] = g_aggs[a].constraints[c];
                }

                // Finally, change pointer of CUDAAggregation struct in shared memory to point to the constraints in shared memory.
                s_aggs[a].constraints = constraints;

                // DONE: Access objects in shared normally via 's_aggs[3].constraints[1].<member>'.
            }
            __syncthreads();
        }

        __device__ void copyDataToShared(tREAL* g_gx, tREAL* s_gx, int F) {

            size_t j = threadIdx.x;

            // copy data to shared gx
            if(threadIdx.y == 0) {
                for(int f = 0; f < F; f++) {
                    s_gx[j * F + f] = g_gx[j * F + f]; // TODO: Attention j might access g_gx out of limit
                }
            }
            __syncthreads();
        }

        __global__ void agg_cuda(const CUDAGroupedData gdata, CUDAAggregation* g_aggs, int A, tREAL* Z, tREAL default_value = 0.0) {

            int a = threadIdx.y; // assume GSY = 1 and BSY = A
            int i = blockDim.x * blockIdx.x + threadIdx.x;

            tUINT N = gdata.nr;
            tUINT F = gdata.nc;
            int gs = gdata.gs[i];
            int gn = gdata.gn[i];
            int ge = gs + gn - 1;

            // memory location for data and aggregations
            tREAL* gx = buffer;
            CUDAAggregation *aggs = (CUDAAggregation *) &gx[blockDim.x * F];

            // copy data and aggregations to shared memory
            copyDataToShared(&gdata.GX[blockDim.x * blockIdx.x * F], gx, F);
            copyAggregationsToShared(g_aggs, aggs, A);

            int ay = aggs[a].target.ay;

            tREAL s_sum = 0.0;
            tREAL s_cnt = 0.0;
            tREAL s_min = 0.0;
            tREAL s_max = 0.0;

            int a_s = 0, a_e = 0;
            int j_s = 0, j_e = 0;

            tREAL *row_i, *row_j;

            if(i < N) {
                row_i = &gx[threadIdx.x * F];

                a_s = i + aggs[a].target.al;
                a_e = i + aggs[a].target.au;
                aggs[a].intersection(gs, ge, a_s, a_e, j_s, j_e); // inplace modification of j_s and j_e

                for(int j = j_s; j <= j_e; j++) {
                    row_j = &gdata.GX[j * F];

                    if(aggs[a].isMatch(row_j, row_i)) {
                        aggs[a].update(row_j[ay], s_cnt, s_sum, s_min, s_max);
                    }
                }
//                Z[i * A + a] = aggs[a].getValue(s_sum, s_cnt, s_min, s_max, default_value);
                Z[gdata.ki[i] * A + a] = aggs[a].getValue(s_sum, s_cnt, s_min, s_max, default_value);
            }
        }
#endif

        void extract(CUDAGroupedData& gdata, std::vector<CUDAAggregation>& aggs, tREAL * h_Z, tREAL default_value, bool fVerbose) {

#ifdef NVCC_COMPILER
            tUINT N = gdata.nr;
            tUINT A = aggs.size();
            int max_devices;
            int device_num;
            cudaDeviceProp props;
            tREAL* d_Z;

            CUDAGroupedData d_gdata = !gdata.onDevice ? copyGroupedDataToDevice(gdata) : gdata; // copy if gdata.device == false, otherwise d_gdata = gdata;
            CUDAAggregation* d_aggs = copyAggregationsToDevice(aggs);

            cuda_check_error(cudaMalloc(&d_Z, sizeof(tREAL) * N * A))
            cuda_check_error(cudaGetDeviceCount(&max_devices))
            device_num = max_devices - 1;
            cuda_check_error(cudaSetDevice(device_num))
            cuda_check_error(cudaGetDeviceProperties(&props, device_num));

            size_t BSX = 32;
            size_t BSY = A;
            size_t GSX = N/BSX + (N % BSX != 0); // beware: compute capability >= 3.0 required to have gridsize > 65535. Otherwise kernel launch fails with error code 229.
            size_t GSY = 1;

            dim3 blockDim (BSX, BSY);
            dim3 gridDim (GSX, GSY);

            size_t n_bytes = BSX * gdata.nc * sizeof(tREAL);
            for(int a = 0; a < A; a++) {
                n_bytes += sizeof(CUDAAggregation) + aggs[a].C * sizeof(CUDAConstraint); // constraint[c].vset excluded
            }

            if(fVerbose)
                printf("DEV %d %s | GRID %d %d | BLOCK %d %d | SHAREDMEM %lu\n", device_num, props.name, gridDim.x, gridDim.y, blockDim.x, blockDim.y, n_bytes);

            agg_cuda<<<gridDim, blockDim, n_bytes>>>(d_gdata, d_aggs, A, d_Z, default_value);

            cuda_check_error(cudaPeekAtLastError())
//            cudaDeviceSynchronize();

            // copy result back to host
            copyZToHost(d_Z, h_Z, N * A);

            // free device memory
            // d_gdata is only freed if we had to allocate it. If input param gdata was on device, we leave it there for consecutive function calls.
            if(!gdata.onDevice) {
                deleteGroupedDataFromDevice(d_gdata);
            }
            deleteAggregationsFromDevice(d_aggs);
            freeFromDevice(d_Z);
//            cudaDeviceSynchronize();
#else
            throw CUDANotAvailableException();
#endif
        }
    }
}

