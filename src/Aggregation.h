//
// Created by Johannes Jurgovsky on 2020-03-01.
//

#ifndef CCFD_AGG_AGGREGATION_H
#define CCFD_AGG_AGGREGATION_H

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include <string>
#include <sstream>
#include <vector>
#include <thread>
#include <assert.h>

#include "NumpyMatrix.h"
#include "GroupedData.h"

#include "agg.h"

using namespace ccfd::cuda;
using namespace std::chrono;

namespace ccfd {
    namespace agg {

        class CTarget {
        private:
            CUDATarget* this_d = nullptr;
            CUDATarget htar;
            CUDATarget dtar;

            void initCUDATarget() {
                htar.ay = ay;
                htar.af = af;
                htar.al = al;
                htar.au = au;
                dtar = htar;
                if(gpu_available())
                    copyTargetToDevice(&this_d, &dtar);
            }

        public:
            int ay = 0;
            AggMode af = AggMode::COUNT;
            int al = 0;
            int au = 0;

            CTarget() = default;
            CTarget(int _ay, AggMode _af, int _al, int _au) : ay(_ay), af(_af), al(_al), au(_au) {
                initCUDATarget();
            }

            CTarget(const CTarget& other) { // copy constructor
                ay = other.ay;
                af = other.af;
                al = other.al;
                au = other.au;
                initCUDATarget();
            }

            CTarget& operator=(const CTarget& other) // copy assignment operator
            {
                if (&other == this)
                    return *this;

                ay = other.ay;
                af = other.af;
                al = other.al;
                au = other.au;
                initCUDATarget();
                return *this;
            }

            ~CTarget() {
                if(gpu_available())
                    deleteTargetFromDevice(this_d);
            }

            std::string toString() {
                std::ostringstream oss;
                oss << "Target ay = " << ay << " af = " << (int)af << " low = " << al << " high = " << au << std::endl;
                return oss.str();
            }

            CUDATarget* getDevicePtr() {
                return this_d;
            }

            CUDATarget& getCUDATarget(Memory storage) {
                if(storage == Memory::DEVICE) {
                    return dtar;
                } else {
                    return htar;
                }
            }

            void print() {
                std::cout << toString();
            }
        };


        class CConstraint {
        private:
            CUDAConstraint* this_d = nullptr;
            CUDAConstraint hcon;
            CUDAConstraint dcon; // struct with pointers to device memory

            void initCUDAConstraint() {
                hcon.ct = ctype;
                hcon.ci = aci;
                hcon.cl = acl;
                hcon.cu = acu;
                hcon.neg = neg;
                hcon.vset = vset.data();
                hcon.vset_n = vset.size();
                dcon = hcon;
                if(gpu_available())
                    copyConstraintToDevice(&this_d, &dcon); // modifies dcon inplace: dcon.vset->devicememory
            }



        public:
            ConstraintType ctype = ConstraintType::DELTA_INTERVAL;
            int aci = -1;
            tREAL acl = 0.0;
            tREAL acu = 0.0;
            std::vector<tREAL> vset;
            int neg = 0;

            CConstraint() = default;
            CConstraint(ConstraintType _ctype, int _aci, tREAL _acl, tREAL _acu, int _neg = 0) : ctype(_ctype), aci(_aci), acl(_acl), acu(_acu), neg(_neg) {
                assert(_ctype == ConstraintType::DELTA_INTERVAL || _ctype == ConstraintType::VALUE_INTERVAL);
                initCUDAConstraint();
            }
            CConstraint(ConstraintType _ctype, int _aci, std::vector<tREAL> _vset, int _neg = 0) : ctype(_ctype), aci(_aci), vset(std::move(_vset)), neg(_neg) {
                assert(_ctype == ConstraintType::VALUE_SET);
                std::sort(vset.begin(), vset.end());
                initCUDAConstraint();
            }

            CConstraint(const CConstraint& other) { // copy constructor
                ctype = other.ctype;
                aci = other.aci;
                acl = other.acl;
                acu = other.acu;
                neg = other.neg;
                vset = other.vset;
                initCUDAConstraint();
            }

            ~CConstraint() {
                if(gpu_available()) {
                    freeFromDevice(dcon.vset);
                    deleteConstraintFromDevice(this_d);
                }
            }

            std::string toString() {
                std::ostringstream oss;
                if(ctype == ConstraintType::DELTA_INTERVAL || ctype == ConstraintType::VALUE_INTERVAL) {
                    oss << "Constraint type = " << (int)ctype << " ci = " << aci << " neg = " << neg << " low = " << acl << " high = " << acu << std::endl;
                } else {
                    oss << "Constraint type = " << (int)ctype << " ci = " << aci << " neg = " << neg << " vset = {";
                    for(auto& v : vset) {
                        oss << v << ", ";
                    }
                    oss << "}" << std::endl;
                }
                return oss.str();
            }

            CUDAConstraint* getDevicePtr() {
                return this_d;
            }

            CUDAConstraint& getCUDAConstraint(Memory storage) {
                if(storage == Memory::DEVICE) {
                    return dcon;
                } else {
                    return hcon;
                }
            }

            void print() {
                std::cout << toString();
            }
        };


        class CAggregation {
        private:
            CUDAAggregation* this_d = nullptr;
            CUDAAggregation hagg;
            CUDAAggregation dagg; // struct with pointers to device memory

            void initCUDAAggregation() {
                hagg.C = constraints.size();
                hagg.target = target.getCUDATarget(Memory::HOST);
                hagg.logic = logic;

                hagg.constraints = new CUDAConstraint[hagg.C];
                std::vector<CUDAConstraint> dcons;
                for(int c = 0; c < hagg.C; c++) {
                    hagg.constraints[c] = constraints[c].getCUDAConstraint(Memory::HOST);
                    dcons.push_back(constraints[c].getCUDAConstraint(Memory::DEVICE));
                }
                dagg = hagg;
                dagg.constraints = dcons.data();
                if(gpu_available())
                    copyAggregationToDevice(&this_d, &dagg); // modifies dagg inplace: dagg.constraints->devicememory
            }

        public:
            CTarget target;
            std::vector<CConstraint> constraints;
            ConstraintLogic logic = ConstraintLogic::AND;

            CAggregation() = default;
            CAggregation(const CTarget& _tar, const std::vector<CConstraint>& _constraints, const ConstraintLogic _logic = ConstraintLogic::AND) : target(_tar), constraints(_constraints), logic(_logic) {
                initCUDAAggregation();
            }

            CAggregation(const CAggregation& other) {
                target = other.target;
                logic = other.logic;
                constraints = other.constraints;
                initCUDAAggregation();
            }

            ~CAggregation() {
                delete[] hagg.constraints;
                if(gpu_available()) {
                    deleteConstraintFromDevice(dagg.constraints);
                    deleteAggregationFromDevice(this_d);
                }
            }

            // expose to python
            void extract(ccfd::data::CGroupedData<tREAL>& gdata, numpy::NumpyMatrix<tREAL>& z, tREAL default_value = 0.0) {
                assert(z.cols() == 1);
                assert(z.rows() == gdata.getNumItems());
                assert(!gdata.isOnDevice());
                _extract_cpu(gdata.getCUDAGroupedData(), z.data(), default_value, 0, 1);
            }

            void _extract_cpu(const CUDAGroupedData& gdata, tREAL* Z, tREAL default_value = 0.0, int a = 0, int A = 1) {

                size_t N = gdata.nr;
                size_t F = gdata.nc;

                int ay = hagg.target.ay;
                int gs = 0, gn = 0, ge = 0;

                tREAL s_cnt = 0.0f, s_sum = 0.0f, s_min = 0.0f, s_max = 0.0f;

                int a_s = 0, a_e = 0;
                int j_s = 0, j_e = 0;

                tREAL *row_i, *row_j;
//                tUINT oidx;

                for(size_t i = 0; i < N; i++) {

                    row_i = &gdata.GX[i * F];

                    s_cnt = 0.0f, s_sum = 0.0f, s_min = 0.0f, s_max = 0.0f;

                    gs = (int)gdata.gs[i];
                    gn = (int)gdata.gn[i];
                    ge = gs + gn - 1;

                    a_s = i + hagg.target.al;
                    a_e = i + hagg.target.au;
                    hagg.intersection(gs, ge, a_s, a_e, j_s, j_e); // inplace modification of j_s and j_e

//                    printf("gs %d gn %d ge %d a_s %d a_e %d j_s %d j_e %d\n", gs, gn, ge, a_s, a_e, j_s, j_e);
                    for(int j = j_s; j <= j_e; j++) {
                        row_j = &gdata.GX[j * F];

                        if(hagg.isMatch(row_j, row_i)) {
                            hagg.update(row_j[ay], s_cnt, s_sum, s_min, s_max);
                        }
                    }
                    Z[gdata.ki[i] * A + a] = hagg.getValue(s_sum, s_cnt, s_min, s_max, default_value);
                }
            }

            std::string toString() {
                std::ostringstream oss;
                oss << target.toString();
                oss << "Constraint logic: " << (int)logic << "\n";
                for(auto& c : constraints) {
                    oss << c.toString();
                }
                oss << std::endl;
                return oss.str();
            }

            CUDAAggregation* getDevicePtr() {
                return this_d;
            }

            CUDAAggregation& getCUDAAggregation(Memory storage) {
                if(storage == Memory::DEVICE) {
                    return dagg;
                } else {
                    return hagg;
                }
            }

            void print() {
                std::cout << toString();
            }
        };

        std::vector<CUDAAggregation> getCUDAAggregations(std::vector<CAggregation*> aggs, Memory storage) {
            std::vector <CUDAAggregation> cudaggs;
            for (auto &a : aggs) {
                cudaggs.push_back(a->getCUDAAggregation(storage));
            }
            return cudaggs;
        }

        std::vector<std::shared_ptr<CAggregation>> getAsPointers(std::vector<CAggregation>& aggs) {
            std::vector<std::shared_ptr<CAggregation>> agg_ptrs;
            for(auto& agg : aggs) {
                agg_ptrs.push_back(std::make_shared<CAggregation>(agg));
            }
            return agg_ptrs;
        }

        void _extract_kernel(CUDAGroupedData& gdata, std::vector<CAggregation*> aggs, numpy::NumpyMatrix<tREAL>& Z, tREAL default_value = 0.0, bool fCuda = false, bool parallel = false, bool fFreeDevice = true, bool fVerbose = false) {
            assert(Z.cols() == aggs.size());
            assert(Z.rows() == gdata.nr);

            tUINT A = aggs.size();
            tUINT N = gdata.nr;
            tUINT G = gdata.ng;


//            std::vector<std::shared_ptr<CAggregation>> m_aggs = getAsPointers(aggs);

            high_resolution_clock::time_point t1, t2;

            if(fVerbose)
                printf("AGGREGATES %lu | TRANSACTIONS %lu | GROUPS %lu | CPU %d (parallel %d) GPU %d\n", A, N, G, !fCuda, !fCuda && parallel, fCuda);

            t1 = high_resolution_clock::now();

            if(fCuda && !gpu_available()) {
                printf("WARNING: GPU not available. Fallback to parallel CPU implementation.\n");
                fCuda = false;
                parallel = true;
            }

            if(fCuda) {
//                cuda::CUDAGroupedData dgd;
//                if(!gdata.onDevice)
//                    dgd = cuda::copyGroupedDataToDevice(gdata);
                std::vector<CUDAAggregation> cudaggs = getCUDAAggregations(aggs, Memory::DEVICE);
                cuda::extract(gdata, cudaggs, Z.data(), default_value, true);
//                if(fFreeDevice) {
//                    cuda::deleteGroupedDataFromDevice(dgd);
//                }
            } else {
                if(parallel) {
                    std::vector<std::shared_ptr<std::thread>> threads;
                    for(tUINT a = 0; a < A; a++) {
                        auto p_th = std::make_shared<std::thread>(std::thread(&ccfd::agg::CAggregation::_extract_cpu, aggs[a], gdata, Z.data(), default_value, a, A));
                        threads.push_back(p_th);
                    }
                    for(tUINT a = 0; a < A; a++) {
                        threads[a]->join();
                    }
                } else {
                    for(tUINT a = 0; a < A; a++) {
                        aggs[a]->_extract_cpu(gdata, Z.data(), default_value, a, A);
                    }
                }
            }

            if(fVerbose) {
                t2 = high_resolution_clock::now();
                duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
                printf("EXECUTION TIME %.5f seconds.\n", time_span.count());
            }
        }


        // Assume rows in X are a sequence of records from the same group
        void extractFromSequence(const numpy::NumpyMatrix<tREAL>& X, std::vector<CAggregation*> aggs, numpy::NumpyMatrix<tREAL>& Z, tREAL default_value = 0.0, bool fCuda = false, bool parallel = false, bool fVerbose = false) {
            CUDAGroupedData h_gdata;
            h_gdata.onDevice = false;
            h_gdata.nr = X.rows();
            h_gdata.nc = X.cols();
            h_gdata.ng = 1;
            h_gdata.ki = new tUINT[X.rows()];
            h_gdata.gs = new tUINT[X.rows()];
            h_gdata.gn = new tUINT[X.rows()];
            h_gdata.GX = X.data();
            for(size_t i = 0; i < X.rows(); i++) {
                h_gdata.ki[i] = i;
                h_gdata.gs[i] = 0;
                h_gdata.gn[i] = X.rows();
            }

            if(fCuda) {
                cuda::CUDAGroupedData gdata = cuda::copyGroupedDataToDevice(h_gdata);
                agg::_extract_kernel(gdata, aggs, Z, default_value, fCuda, parallel, true, fVerbose);
                cuda::deleteGroupedDataFromDevice(gdata);
            } else {
                agg::_extract_kernel(h_gdata, aggs, Z, default_value, fCuda, parallel, true, fVerbose);
            }

            delete[] h_gdata.ki;
            delete[] h_gdata.gs;
            delete[] h_gdata.gn;
        }






        class CAggregator {
        private:
            ccfd::data::CGroupedData<tREAL> m_groupedData;
        public:
            CAggregator() = default;

            CAggregator(const numpy::NumpyMatrix<tREAL>& X, long groupByCol) {
                ccfd::data::CGroupedData<tREAL> data(X, groupByCol, true);
                m_groupedData = data;
//                m_groupedData.copyToDevice();
            };

            void extract(std::vector<CAggregation*> aggs, numpy::NumpyMatrix<tREAL>& Z, tREAL default_value = 0.0, bool fCuda = false, bool parallel = false, bool fFreeDevice = true, bool fVerbose = false) {
                assert(m_groupedData.isContiguous());
                if(fCuda)
                    m_groupedData.copyToDevice();
                cuda::CUDAGroupedData& gdata = m_groupedData.getCUDAGroupedData();
                agg::_extract_kernel(gdata, aggs, Z, default_value, fCuda, parallel, fFreeDevice, fVerbose);
                if(fCuda && fFreeDevice) {
                    m_groupedData.deleteFromDevice();
                }
            }

            std::vector<tUINT> getGroup(tREAL group) {
                return m_groupedData.getGroup(group);
            }

            long getPredecessor(tUINT index) {
                return m_groupedData.getPredecessor(index);
            }

            long getSuccessor(tUINT index) {
                return m_groupedData.getSuccessor(index);
            }

            std::vector<tUINT> getPredecessorsOf(tUINT index) {
                return m_groupedData.getPredecessorsOf(index);
            }

            std::vector<tUINT> getSuccessorsOf(tUINT index) {
                return m_groupedData.getSuccessorsOf(index);
            }

            tUINT getNumGroups() {
                return m_groupedData.getNumGroups();
            }

            tUINT getNumItems() {
                return m_groupedData.getNumItems();
            }

            long getGroupColIndex() {
                return m_groupedData.getGroupColIndex();
            }
        };
    }
}

#endif //CCFD_AGG_AGGREGATION_H



// OLD EXTRACT

//            // internal usage
//            void _extract(ccfd::data::CGroupedData<tREAL>& gdata, tREAL* Z, tREAL default_value = 0.0, int a = 0, int A = 1) {
//                numpy::NumpyMatrix<tREAL>& X = gdata.data();
//
//                size_t _row_size = X.cols();
//
//                tREAL s_cnt = 0.0f, s_sum = 0.0f, s_min = 0.0f, s_max = 0.0f;
//                long j, s_s, s_e;
//                tREAL *row_i, *row_j;
//                tUINT oidx;
//
//                for(size_t i = 0; i < X.rows(); i++) {
//
//                    row_i = &X.data()[i * _row_size];
//                    s_cnt = 0.0f, s_sum = 0.0f, s_min = 0.0f, s_max = 0.0f;
//
//                    s_s = gdata.getFirstInOffsetInterval(i, target.al, target.au);
//                    s_e = gdata.getLastInOffsetInterval(i, target.al, target.au);
//
//                    j = s_s;
//                    while(j != -1 && j <= s_e) {
//                        row_j = &X.data()[j * _row_size];
//                        if (isMatch(row_j, row_i)) {
//                            update(X.get(j, target.ay), s_cnt, s_sum, s_min, s_max);
//                        }
//                        j = gdata.getSuccessorOf(j);
//                    }
//
//                    oidx = gdata.getOriginalIndex(i);
////                    z[oidx] = getValue(s_cnt, s_sum, s_min, s_max, default_value);
//                    Z[oidx * A + a] = getValue(s_cnt, s_sum, s_min, s_max, default_value);
//
//                }
//            }

//                assert(Z.cols() == aggs.size());
//                assert(Z.rows() == m_groupedData.getNumItems());
//                assert(m_groupedData.isContiguous());
//
//                tUINT A = aggs.size();
//                tUINT N = m_groupedData.getNumItems();
//
//                if(fCuda && !m_groupedData.isOnDevice()) {
//                    if(!m_groupedData.copyToDevice()) {
//                        return;
//                    }
//                }
//
//                cuda::CUDAGroupedData& dgd = m_groupedData.getCUDAGroupedData();
//                std::vector<std::shared_ptr<CAggregation>> m_aggs = getAsPointers(aggs);
//
//                high_resolution_clock::time_point t1, t2;
//
//                if(fVerbose)
//                    printf("AGGREGATES %lu | TRANSACTIONS %lu | GROUPS %lu | CPU %d (parallel %d) GPU %d\n", A, N, m_groupedData.getNumGroups(), !fCuda, !fCuda && parallel, fCuda);
//
//                t1 = high_resolution_clock::now();
//
//                if(fCuda) {
//                    std::vector<CUDAAggregation> cudaggs = getCUDAAggregations(aggs, Memory::DEVICE);
//                    cuda::extract(dgd, cudaggs, Z.data(), default_value, true);
//                    if(fFreeDevice) {
//                        m_groupedData.deleteFromDevice();
//                    }
//                } else {
//                    if(parallel) {
//                        std::vector<std::shared_ptr<std::thread>> threads;
//                        for(tUINT a = 0; a < A; a++) {
//                            auto p_th = std::make_shared<std::thread>(std::thread(&ccfd::agg::CAggregation::_extract_cpu, m_aggs[a], dgd, Z.data(), default_value, a, A));
//                            threads.push_back(p_th);
//                        }
//                        for(tUINT a = 0; a < A; a++) {
//                            threads[a]->join();
//                        }
//                    } else {
//                        for(tUINT a = 0; a < A; a++) {
//                            m_aggs[a]->_extract_cpu(dgd, Z.data(), default_value, a, A);
//                        }
//                    }
//                }
//
//                if(fVerbose) {
//                    t2 = high_resolution_clock::now();
//                    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
//                    printf("EXECUTION TIME %.5f seconds.\n", time_span.count());
//                }

//
//void test() {
//    CTarget tar {0, AggMode::MIN, -100, 0};
//    CConstraint co1(ConstraintType::DELTA_INTERVAL, 2, -1.0, 1.0);
//    std::vector<tREAL> vset = {19.0, -1.0, 23.0, 0.0, -4.888};
//    CConstraint co2(ConstraintType::VALUE_SET, 2, vset);
//    std::vector<CConstraint> colist = {co1, co2};
//    CAggregation agg {tar, colist, ConstraintLogic::OR};
//
//
//    CTarget tar2 {2, AggMode::MEAN, -101, 2};
//    CConstraint co21{ConstraintType::VALUE_INTERVAL, 0, 50, 100};
//    CConstraint co22{ConstraintType::VALUE_SET, 1, vset};
//    std::vector<CConstraint> colist2 = {co21, co22};
//    CAggregation agg2 {tar2, colist2, ConstraintLogic::OR};
//
//    std::vector<CAggregation> aggs = {agg, agg2};
//    std::vector<CUDAAggregation> cudaggs = getCUDAAggregations(aggs, Memory::DEVICE);
//
//    int A = aggs.size();
//    CUDAAggregation* d_aggs = copyAggregationsToDevice(cudaggs);
//
////            test(tar.getDevicePtr(), co1.getDevicePtr(), co2.getDevicePtr(), agg.getDevicePtr(), d_aggs, A);
//
//}