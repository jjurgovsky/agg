//
// Created by Johannes Jurgovsky on 2020-01-21.
//

#ifndef CCFD_DATA_GROUPEDDATA_H
#define CCFD_DATA_GROUPEDDATA_H

#include <vector>
#include <unordered_map>
#include <iostream>
#include <assert.h>

#include "NumpyMatrix.h"
#include "agg.h"

namespace ccfd {
    namespace data {
        /*
        * Build a lookup table for group-wise predecessors and successors of all rows of the input matrix.
        * Input: Data matrix and a column index to denote which column should serve as group column.
        *
        *    e.g. with column index = 1 we treat the values in the second column of the DataMatrix as groups and
        *    record the predecessor and the successor of each row. A value of -1 indicates no predecessor/successor:
        *
        *         rowIdx  DataMatrix       predecessors, successors
        *           0     0.33  1.1             -1            2
                    1     0.31  2.2             -1            3
                    2     0.42  1.1    --->      0            5
                    3     0.24  2.2              1            4
                    4     0.21  2.2              3           -1
                    5     0.83  1.1              2           -1
        */

        template <class T>
        class CGroupedData {

        private:
//            Matrix<T> m_dataset;
            numpy::NumpyMatrix<T> m_dataset;


            long m_gi = -1;
            bool m_isContiguous = false;

            std::vector<T> groups;
            std::unordered_map<T, tUINT> ggs;

            std::vector<tUINT> ik;
            std::vector<tUINT> ki;

            std::vector<tUINT> kgs;
            std::vector<tUINT> kgn;

            cuda::CUDAGroupedData cgd;

//            void build() {
//                std::unordered_map<T, std::vector<tUINT>> mm;
//
//                T g;
//                size_t N = m_dataset.rows();
//
//                ik.resize(N);
//                ki.resize(N);
//                kgs.resize(N);
//                kgn.resize(N);
//
//                for(tUINT i = 0; i < N; i++) {
//                    g = m_dataset(i, m_gi);
//                    if(mm.find(g) == mm.end()) {
//                        mm[g] = std::vector<tUINT>();
//                        groups.push_back(g);
//                    }
//                    mm[g].push_back(i);
//                }
//
//                tUINT k = 0;
//                tUINT _kgs = 0;
//                tUINT _kgn = 0;
//                for(auto gr : groups) {
//                    _kgs = k;
//                    ggs[gr] = _kgs;
//                    _kgn = mm[gr].size();
//                    for(auto i : mm[gr]) {
//                        ik[i] = k;
//                        ki[k] = i;
//                        kgs[k] = _kgs;
//                        kgn[k] = _kgn;
//                        k++;
//                    }
//                }
//            }

            void build() {
                std::unordered_map<T, std::vector<tUINT>> mm;

                T g;
                size_t N = m_dataset.rows();

                ik.resize(N);
                ki.resize(N);
                kgs.resize(N);
                kgn.resize(N);

                for(tUINT i = 0; i < N; i++) {
                    g = m_dataset.get(i, m_gi);
                    if(mm.find(g) == mm.end()) {
                        mm[g] = std::vector<tUINT>();
                        groups.push_back(g);
                    }
                    mm[g].push_back(i);
                }

                tUINT k = 0;
                tUINT _kgs = 0;
                tUINT _kgn = 0;
                for(auto gr : groups) {
                    _kgs = k;
                    ggs[gr] = _kgs;
                    _kgn = mm[gr].size();
                    for(auto i : mm[gr]) {
                        ik[i] = k;
                        ki[k] = i;
                        kgs[k] = _kgs;
                        kgn[k] = _kgn;
                        k++;
                    }
                }
            }

            void initCUDAGroupedData() {
                cgd.onDevice = false;
                cgd.nr = m_dataset.rows();
                cgd.nc = m_dataset.cols();
                cgd.ng = groups.size();
                cgd.ki = ki.data();
                cgd.gs = kgs.data();
                cgd.gn = kgn.data();
                cgd.GX = m_dataset.data();
            }

        public:
            CGroupedData() = default;
            ~CGroupedData() {
                if(cgd.onDevice) {
                    if(gpu_available())
                        cuda::deleteGroupedDataFromDevice(cgd);
                }
            }


            CGroupedData(const numpy::NumpyMatrix<T>& dataset, long groupColIndex, bool createSortedCopy=false) {
                m_gi = groupColIndex;
                m_isContiguous = createSortedCopy;
                m_dataset = dataset;
                this->build();

                if(createSortedCopy) {
                    numpy::NumpyMatrix<T> mat1(dataset.rows(), dataset.cols());

//                    Matrix<T> mat1 = Matrix<T>::Zero(dataset.rows(), dataset.cols());
                    for(tUINT i = 0; i < ik.size(); i++) {
                        for(tUINT j = 0; j < dataset.cols(); j++) {
                            mat1.set(ik[i], j, dataset.get(i, j));
                        }
                    }
                    m_dataset = mat1;
                }

                initCUDAGroupedData();
            }

            CGroupedData(const CGroupedData& gdata) {
                m_gi = gdata.m_gi;
                m_isContiguous = gdata.m_isContiguous;
                m_dataset = gdata.m_dataset;
                groups = gdata.groups;
                ggs = gdata.ggs;
                ik = gdata.ik;
                ki = gdata.ki;
                kgs = gdata.kgs;
                kgn = gdata.kgn;
                initCUDAGroupedData();
            }

            CGroupedData& operator=( const CGroupedData& gdata ) { // copy assignment
                if(&gdata == this)
                    return *this;

                m_gi = gdata.m_gi;
                m_isContiguous = gdata.m_isContiguous;
                m_dataset = gdata.m_dataset;
                groups = gdata.groups;
                ggs = gdata.ggs;
                ik = gdata.ik;
                ki = gdata.ki;
                kgs = gdata.kgs;
                kgn = gdata.kgn;
                initCUDAGroupedData();
                return *this;
            }

            inline tUINT getK(tUINT item) {
                return m_isContiguous ? item : ik[item];
//                return ik[item];
            }

            inline tUINT getI(tUINT ktem) {
//                return ki[ktem];
                return m_isContiguous ? ktem : ki[ktem];
            }

            inline tUINT getOriginalIndex(tUINT groupedIndex) {
                return m_isContiguous ? ki[groupedIndex] : groupedIndex;
            }

            // internal use only, assumes item indexes into the m_dataset container
            inline long getPredecessorOf(tUINT item) {
                tUINT k = getK(item);
                return k > kgs[k] ? getI(k - 1) : -1;
            }

            // internal use only, assumes item indexes into the m_dataset container
            inline long getSuccessorOf(tUINT item) {
                tUINT k = getK(item);
                return k < kgs[k] + kgn[k]-1 ? getI(k + 1) : -1;
            }

            long getPredecessor(tUINT item) {
                tUINT k = ik[item];
                return k > kgs[k] ? ki[k-1] : -1;
            }

            long getSuccessor(tUINT item) {
                tUINT k = ik[item];
                return k < kgs[k] + kgn[k] - 1 ? ki[k+1] : -1;
            }

            std::vector<tUINT> getPredecessorsOf(tUINT item) {
                std::vector<tUINT> tmp;
                long pi = getPredecessor(item);
                while(pi != -1) {
                    tmp.insert(tmp.begin(), pi);
                    pi = getPredecessor(pi);
                }
                return tmp;
            }

            std::vector<tUINT> getSuccessorsOf(tUINT item) {
                std::vector<tUINT> tmp;
                long si = getSuccessor(item);
                while(si != -1) {
                    tmp.push_back(si);
                    si = getSuccessor(si);
                }
                return tmp;
            }

            std::vector<tUINT> getGroup(T group) {
                if(ggs.find(group) == ggs.end()) {
                    return std::vector<tUINT>();
                }
                tUINT _kgs = ggs[group];
                tUINT n = kgn[_kgs];
                std::vector<tUINT> tmp(n);
                for(tUINT k = 0; k < n; k++) {
                    tmp[k] = getOriginalIndex(_kgs + k);
                }
                return tmp;
            }

            inline long getFirstInOffsetInterval(tUINT item, long offset_low, long offset_high) {
                tUINT k = getK(item);
                long n = kgn[k];
                tUINT _kgs = kgs[k];
                tUINT r = k - _kgs;                                         // r \in [0, kgn[k]-1]
                long r_low = r + offset_low, r_high = r + offset_high;
                if(r_high < 0 || r_low >= n) return -1;                     // [0,n-1] \intersect [r_low, r_high] = \emptyset
                return r_low > 0 ? getI(_kgs + r_low) : getI(_kgs);         // getI(_kgs + max(0, r_low))
            }

            inline long getLastInOffsetInterval(tUINT item, long offset_low, long offset_high) {
                tUINT k = getK(item);
                long n = kgn[k];
                tUINT _kgs = kgs[k];
                tUINT r = k - _kgs;                                              // r \in [0, n-1]
                long r_low = r + offset_low, r_high = r + offset_high;
                if(r_high < 0 || r_low >= n) return -1;                          // [0,n-1] \intersect [r_low, r_high] = \emptyset
                return r_high < n-1 ? getI(_kgs + r_high) : getI(_kgs + n - 1);  // getI(_kgs + min(n-1, r_high))
            }

//            void restoreOrder(MatrixCM<T>& Z) {
//                assert(Z.rows() == ki.size());
//                if(m_isContiguous) {
//                    MatrixCM<T> tmp = MatrixCM<T>::Zero(Z.rows(), Z.cols());
//                    for(tUINT k = 0; k < Z.rows(); k++) {
//                        for(tUINT c = 0; c < Z.cols(); c++) {
//                            tmp(ki[k], c) = Z(k, c);
//                        }
//                    }
//                    for(tUINT k = 0; k < Z.rows(); k++) {
//                        for(tUINT c = 0; c < Z.cols(); c++) {
//                            Z(k,c) = tmp(k,c);
//                        }
//                    }
//                }
//            }

//            template <typename TD>
//            void restoreOrder(TD* z, tUINT n) {
//                assert(n == ki.size());
//                if(m_isContiguous) {
//                    TD* tmp = new TD[n];
//                    for(tUINT k = 0; k < n; k++) {
//                        tmp[ki[k]] = z[k];
//                    }
//                    for(tUINT k = 0; k < n; k++) {
//                        z[k] = tmp[k];
//                    }
//                    delete[] tmp;
//                }
//            }

            void printGroup(T group) {
                std::cout << "Group: " << group << " Items: ";
                auto items = getGroup(group);
                for(auto& item : items) {
                    std::cout << item << std::endl;
                }
            }

            void printMap() {
                std::cout << "GroupedData for column: " << m_gi << std::endl;
                for(auto const& entry : ggs) {
                    printGroup(entry.first);
                }
            }

            bool copyToDevice() {
                if(!gpu_available())
                    return false;

                if(!cgd.onDevice) {
                    cuda::CUDAGroupedData dgd;
                    try {
                        dgd = cuda::copyGroupedDataToDevice(cgd);
                        this->cgd = dgd;
                        return true;
                    } catch (...) {
                        std::cout << "ERROR: Copying CUDAGroupedData to CUDA device failed" << std::endl;
                        return false;
                    }
                } else {
                    std::cout << "INFO: CUDAGroupedData already in CUDA device memory." << std::endl;
                    return true;
                }
            }

            bool isOnDevice() {
                return cgd.onDevice;
            }

            bool deleteFromDevice() {
                if(!gpu_available())
                    return false;

                if(cgd.onDevice) {
                    cuda::deleteGroupedDataFromDevice(cgd);
                    initCUDAGroupedData();
                    return true;
                } else {
                    initCUDAGroupedData();
                    return false;
                }
            }

            cuda::CUDAGroupedData& getCUDAGroupedData() {
                return cgd;
            }

            numpy::NumpyMatrix<T>& data() { return m_dataset; }
            tUINT getNumGroups() { return ggs.size(); }
            tUINT getNumItems() { return m_dataset.rows(); }
            long getGroupColIndex() { return m_gi; }
            bool isContiguous() { return m_isContiguous; }
        };
    }
}

// Holy crap, the separation of declaration and implementation with template classes does not work.
// If member functions of template classes are implemented in a separate cpp file, the linker will throw errors.
// The standard "hack" is to just include the implementation in the header file.
// Make sure to exclude the .cpp file from the build. Otherwise you get duplicate symbols errors.
//#include "GroupedData.cpp"

#endif //CCFD_DATA_GROUPEDDATA_H
