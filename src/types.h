//
// Created by Johannes Jurgovsky on 2020-02-18.
//

#ifndef CCFD_SIMPLETYPES_H
#define CCFD_SIMPLETYPES_H


typedef float tREAL;
typedef long tINT;
typedef unsigned long tUINT;

#endif //CCFD_SIMPLETYPES_H
