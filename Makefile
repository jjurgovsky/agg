#VERSION = 0.0.1
#CC      = nvcc
#CFLAGS  = -Wno-deprecated-gpu-targets -DVERSION=\"$(VERSION)\" -std=c++11 -Xcompiler "-fPIC -O3 -mmacosx-version-min=10.12" -gencode arch=compute_30,code=sm_30 --use_fast_math -m 64
#LDFLAGS =
#
#libagg.a: agg.o
#	ar rvs build/libagg.a build/agg.o
#	rm build/agg.o
#
#agg.o: src/agg.cu
#	$(CC) $(CFLAGS) -c src/agg.cu -o build/agg.o



ifneq (, $(shell which nvcc))
CC = nvcc
CFLAGS = -Wno-deprecated-gpu-targets -DVERSION=\"$(VERSION)\" -std=c++11 -Xcompiler "-fPIC -O3" -gencode arch=compute_30,code=sm_30 -O3 --use_fast_math -m 64
FORCE_CPP =
else
$(warning CUDA compiler 'nvcc' not found. Compiling with g++ instead. Extension will fall back to CPU implementation.)
CC		= g++
CFLAGS	= -std=c++11 -O3 -fPIC
FORCE_CPP = -x c++
endif

libagg.a: agg.o
	ar rvs build/libagg.a build/agg.o
	rm build/agg.o

agg.o: src/agg.cu
	mkdir -p build
	$(CC) $(CFLAGS) $(FORCE_CPP) -c src/agg.cu -o build/agg.o


sdist:
	cython cyth/agg.pyx -o cyth/agg.cython.cpp --cplus -3 -f
	python setup.py sdist